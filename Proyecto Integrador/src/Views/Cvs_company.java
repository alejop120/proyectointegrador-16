package Views;
import Classes.Cv_company;
import Classes.ConexionSQL;
import Lists.Simple_list_cv_company;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author libardo
 */
public class Cvs_company extends javax.swing.JFrame {
    DefaultTableModel model_list;

    public Cvs_company(){
      initComponents();
      this.setTitle("Cv Company");
      this.setLocationRelativeTo(this);
      
      model_list = new DefaultTableModel(){
      @Override
        public boolean isCellEditable(int row, int column){
          Object first = table_cv.getValueAt(row, 0);
          Object second = table_cv.getValueAt(row, 1);
          Object third = table_cv.getValueAt(row, 2);
          Object fourth = table_cv.getValueAt(row, 3);
          Object fifth = table_cv.getValueAt(row, 4);

          String[] fields = new String[5];

          fields[0] = first.toString();
          fields[1] = second.toString();
          fields[2] = third.toString();
          fields[3] = fourth.toString();
          fields[4] = fifth.toString();

          return true;
        }
      };
    
      model_list.addColumn("Name");
      model_list.addColumn("Surname");
      model_list.addColumn("Document");
      model_list.addColumn("Position");
      model_list.addColumn("Requested position");

      this.table_cv.setModel(model_list);
      table_cv.getColumnModel().getColumn(0).setPreferredWidth(90);
      table_cv.getColumnModel().getColumn(1).setPreferredWidth(90);
      table_cv.getColumnModel().getColumn(2).setPreferredWidth(90);
      table_cv.getColumnModel().getColumn(3).setPreferredWidth(90);
      table_cv.getColumnModel().getColumn(4).setPreferredWidth(90);
    }
    
    Simple_list_cv_company new_cv = new Simple_list_cv_company();
    
    public void clear(){
      text_name_cv.setText(null);
      text_surname_cv.setText(null);
      text_document_cv.setText(null);
      text_position_cv.setText(null);
      text_requested_cv.setText(null);
    }
    
    public void clean_table(){
      try{
         int rows = table_cv.getRowCount();
          if (rows > 0){
            for(int i = 0; rows > i; i++){
               model_list.removeRow(0);
            }
          }
      }
      catch(Exception e){
        JOptionPane.showMessageDialog(null, "Failed to clean the table.");
      }
    }
    
    public void add_elements(){
      if(text_name_cv.getText().length() > 0 && text_surname_cv.getText().length() > 0 && text_document_cv.getText().length() > 0 && text_position_cv.getText().length() > 0 && text_requested_cv.getText().length() > 0){
        Cv_company cv = new Cv_company(text_name_cv.getText(), text_surname_cv.getText(), Integer.parseInt(text_document_cv.getText()), text_position_cv.getText(), text_requested_cv.getText());
        new_cv.insert(cv);
        if(!new_cv.is_empty()){
          clean_table();
          for(int i = 0; i < new_cv.size(); i++){
            model_list.addRow(new Object[]{new_cv.display(i).cv.getName(), new_cv.display(i).cv.getSurname(), new_cv.display(i).cv.getDocument(), new_cv.display(i).cv.getPosition(), new_cv.display(i).cv.getRequested_position()});
          } 
        }
        clear();
        JOptionPane.showMessageDialog(null, "Create successfully");  
      }
      else{
        JOptionPane.showMessageDialog(null, "The data are required for create.");
      }
    }
    
    public void delete(){
      if(text_name_cv.getText().length() > 0 && text_surname_cv.getText().length() > 0 && text_document_cv.getText().length() > 0 && text_position_cv.getText().length() > 0 && text_requested_cv.getText().length() > 0){
       new_cv.delete_list(text_name_cv.getText(), text_surname_cv.getText(), Integer.parseInt(text_document_cv.getText()), text_position_cv.getText(), text_requested_cv.getText());
       clear();
       clean_table();
        if(!new_cv.is_empty()){
          for(int i = 0; i < new_cv.size(); i++){
            model_list.addRow(new Object[]{new_cv.display(i).cv.getName(), new_cv.display(i).cv.getSurname(), new_cv.display(i).cv.getDocument(), new_cv.display(i).cv.getPosition(), new_cv.display(i).cv.getRequested_position()});
          }   
        }  
      }
      else{
          JOptionPane.showMessageDialog(null, "Enter the data for delete the Cv.");
      }
    }
    
    public void update(){
      if(!new_cv.is_empty()){
        if(text_search_cv.getText().length() > 0){
           Cv_company cv = new Cv_company(text_name_cv.getText(), text_surname_cv.getText(), Integer.parseInt(text_document_cv.getText()), text_position_cv.getText(), text_requested_cv.getText());
           new_cv.update(text_search_cv.getText(), cv);
            clean_table();
            for(int i = 0; i < new_cv.size(); i++){
              model_list.addRow(new Object[]{new_cv.display(i).cv.getName(), new_cv.display(i).cv.getSurname(), new_cv.display(i).cv.getDocument(), new_cv.display(i).cv.getPosition(), new_cv.display(i).cv.getRequested_position()});
            } 
           clear();
           JOptionPane.showMessageDialog(null, "Updated successfully.");
        }
        else{
          JOptionPane.showMessageDialog(null, "Search the cv to update.");
        }   
      }
      else{
          JOptionPane.showMessageDialog(null, "The list is empty.");
      }
    }
    
    public void search_elements(String search){ 
      if(search != null){
          if(new_cv.is_empty()){
            JOptionPane.showMessageDialog(null, "The list is empty" );
          }
          else{
            text_name_cv.setText(new_cv.search_index(search).cv.getName());
            text_surname_cv.setText(new_cv.search_index(search).cv.getSurname());
            text_document_cv.setText(String.valueOf(new_cv.search_index(search).cv.getDocument()));
            text_position_cv.setText(new_cv.search_index(search).cv.getPosition());
            text_requested_cv.setText(new_cv.search_index(search).cv.getRequested_position());
            clean_table();
            model_list.addRow(new Object[]{text_name_cv.getText(), text_surname_cv.getText(), Integer.parseInt(text_document_cv.getText()), text_position_cv.getText(), text_requested_cv.getText()});
          }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the data");
      } 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        btn_back_cv = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        text_search_cv = new javax.swing.JTextField();
        btn_search_cv = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        text_name_cv = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        text_surname_cv = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        text_document_cv = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        text_position_cv = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        text_requested_cv = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        btn_file_cv = new javax.swing.JButton();
        btn_create_cv = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_cv = new javax.swing.JTable();
        btn_update = new javax.swing.JButton();
        btn_delete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(650, 500));
        setPreferredSize(new java.awt.Dimension(650, 500));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N
        panelImage1.setPreferredSize(new java.awt.Dimension(650, 500));

        btn_back_cv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back_cv.setMaximumSize(new java.awt.Dimension(49, 42));
        btn_back_cv.setMinimumSize(new java.awt.Dimension(49, 42));
        btn_back_cv.setPreferredSize(new java.awt.Dimension(49, 42));
        btn_back_cv.setSize(new java.awt.Dimension(49, 42));
        btn_back_cv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_back_cvActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Cv Company");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel2.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 14)); // NOI18N
        jLabel2.setText(" Search:");

        btn_search_cv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        btn_search_cv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search_cvActionPerformed(evt);
            }
        });

        jLabel3.setText(" Name:");

        jLabel4.setText(" Surname: ");

        jLabel5.setText(" Document:");

        jLabel6.setText(" Position:");

        jLabel7.setText(" Requested position:  ");

        jLabel8.setText(" File:");

        btn_file_cv.setText("Search File");

        btn_create_cv.setText("Create");
        btn_create_cv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_create_cvActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(table_cv);

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });

        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addComponent(btn_back_cv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(text_search_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(btn_search_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(227, 227, 227))))
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE))
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btn_create_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)))
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(text_surname_cv, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                            .addComponent(text_name_cv, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(text_requested_cv))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(text_document_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(text_position_cv)
                                    .addComponent(btn_file_cv, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)))))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(btn_update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_delete)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_back_cv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_search_cv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(text_search_cv)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(text_name_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(text_document_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(text_surname_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(text_position_cv, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(text_requested_cv)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_file_cv)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_create_cv)
                    .addComponent(btn_update)
                    .addComponent(btn_delete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(354, 354, 354))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, 657, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_back_cvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_back_cvActionPerformed
      dispose();
      new Selection_program().setVisible(true);
    }//GEN-LAST:event_btn_back_cvActionPerformed

    private void btn_create_cvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_create_cvActionPerformed
      add_elements();
    }//GEN-LAST:event_btn_create_cvActionPerformed

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
      update();
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
      delete();
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void btn_search_cvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_cvActionPerformed
      if(text_search_cv.getText().length() > 0){
         if(!new_cv.is_empty()){
           search_elements(text_search_cv.getText());   
         }
         else{
           JOptionPane.showMessageDialog(null, "List of Cvs are empty");
         }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the data for search.");
      }
    }//GEN-LAST:event_btn_search_cvActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Cvs_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Cvs_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Cvs_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cvs_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cvs_company().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back_cv;
    private javax.swing.JButton btn_create_cv;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_file_cv;
    private javax.swing.JButton btn_search_cv;
    private javax.swing.JButton btn_update;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JTable table_cv;
    private javax.swing.JTextField text_document_cv;
    private javax.swing.JTextField text_name_cv;
    private javax.swing.JTextField text_position_cv;
    private javax.swing.JTextField text_requested_cv;
    private javax.swing.JTextField text_search_cv;
    private javax.swing.JTextField text_surname_cv;
    // End of variables declaration//GEN-END:variables
}
