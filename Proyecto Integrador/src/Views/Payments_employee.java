package Views;
import Classes.ConexionSQL;
import Classes.Payment;
import Lists.Simple_list_payment;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author libardo
 */
public class Payments_employee extends javax.swing.JFrame {
    DefaultTableModel model;
    DefaultTableModel model_list;
    String tableName = "payment";
    public Payments_employee(){
      initComponents();
      jTextFieldEmployeeId.setVisible(false);
      jTextFieldId.setVisible(false);
      this.setTitle("Payments Employed");
      this.setLocationRelativeTo(this);
      
      model_list = new DefaultTableModel(){
      @Override
        public boolean isCellEditable(int row, int column){
          Object first = table_pay.getValueAt(row, 0);
          Object second = table_pay.getValueAt(row, 1);
          Object third = table_pay.getValueAt(row, 2);
          Object fourth = table_pay.getValueAt(row, 3);
          Object fifth = table_pay.getValueAt(row, 4);

          String[] fields = new String[5];

          fields[0] = first.toString();
          fields[1] = second.toString();
          fields[2] = third.toString();
          fields[3] = fourth.toString();
          fields[4] = fifth.toString();

          return true;
        }
      };
    
      model_list.addColumn("Concept");
      model_list.addColumn("Observation");
      model_list.addColumn("Value");
      model_list.addColumn("Date");
      model_list.addColumn("Refunded");

      this.table_pay.setModel(model_list);
      table_pay.getColumnModel().getColumn(0).setPreferredWidth(100);
      table_pay.getColumnModel().getColumn(1).setPreferredWidth(100);
      table_pay.getColumnModel().getColumn(2).setPreferredWidth(70);
      table_pay.getColumnModel().getColumn(3).setPreferredWidth(100);
      table_pay.getColumnModel().getColumn(4).setPreferredWidth(100);
    }
    
    Simple_list_payment new_pay = new Simple_list_payment();
    
    public void clear(){
      textConcept.setText(null);
      textObservation.setText(null);
      paymentDate.setDate(null);
      textValue.setText(null);
      checkBoxRefunded.setText(null);
    }
    
    public void clean_table(){
      try{
         int rows = table_pay.getRowCount();
          if (rows > 0){
            for(int i = 0; rows > i; i++){
               model_list.removeRow(0);
            }
          }
      }
      catch(Exception e){
        JOptionPane.showMessageDialog(null, "Failed to clean the table.");
      }
    }
    
    public void add_elements(){
      if(textConcept.getText().length() > 0 && textObservation.getText().length() > 0 && paymentDate.getDate().toString().length() > 0 && textValue.getText().length() > 0 && checkBoxRefunded.getText().length() > 0){
        Date dateFromDateChooser = paymentDate.getDate();
        String date = String.format("%1$td-%1$tm-%1$tY", dateFromDateChooser);
        Payment pay = new Payment(textConcept.getText(), textObservation.getText(), Integer.parseInt(textValue.getText()), date, Boolean.valueOf(checkBoxRefunded.getText()));
        new_pay.insert(pay);
        if(!new_pay.is_empty()){
          clean_table();
          for(int i = 0; i < new_pay.size(); i++){
            model_list.addRow(new Object[]{new_pay.display(i).payment.getConcept(), new_pay.display(i).payment.getObservations(), new_pay.display(i).payment.getValue(), new_pay.display(i).payment.getDate(), new_pay.display(i).payment.getRefunded()});
          } 
        }
//        clear();
        JOptionPane.showMessageDialog(null, "Create successfully");  
      }
      else{
        JOptionPane.showMessageDialog(null, "The data are required for create.");
      }
    }
    
    public void delete(){
      if(textConcept.getText().length() > 0 && textObservation.getText().length() > 0 && textValue.getText().length() > 0 ){
       new_pay.delete_list(textConcept.getText(), textObservation.getText(), Integer.parseInt(textValue.getText()) );
       clear();
       clean_table();
        if(!new_pay.is_empty()){
          for(int i = 0; i < new_pay.size(); i++){
            model_list.addRow(new Object[]{new_pay.display(i).payment.getConcept(), new_pay.display(i).payment.getObservations(), new_pay.display(i).payment.getValue(), new_pay.display(i).payment.getDate(), new_pay.display(i).payment.getRefunded()});
          }   
        }  
      }
      else{
          JOptionPane.showMessageDialog(null, "Enter the data for delete the pay.");
      }
    }
    
    public void update(){
      if(!new_pay.is_empty()){
        if(text_payment_search.getText().length() > 0){
           Date dateFromDateChooser = paymentDate.getDate();
           String date = String.format("%1$td-%1$tm-%1$tY", dateFromDateChooser);
           Payment pay = new Payment(textConcept.getText(), textObservation.getText(), Integer.parseInt(textValue.getText()), date, Boolean.valueOf(checkBoxRefunded.getText()));
           new_pay.update(text_payment_search.getText(), pay);
            clean_table();
            for(int i = 0; i < new_pay.size(); i++){
              model_list.addRow(new Object[]{new_pay.display(i).payment.getConcept(), new_pay.display(i).payment.getObservations(), new_pay.display(i).payment.getValue(), new_pay.display(i).payment.getDate(), new_pay.display(i).payment.getRefunded()});
            } 
           clear();
           JOptionPane.showMessageDialog(null, "Updated successfully.");
        }
        else{
          JOptionPane.showMessageDialog(null, "Search the pay to update.");
        }   
      }
      else{
          JOptionPane.showMessageDialog(null, "The list is empty.");
      }
    }
    
    public void search_elements(String search){ 
      if(search != null){
          if(new_pay.is_empty()){
            JOptionPane.showMessageDialog(null, "The list is empty" );
          }
          else{
            textConcept.setText(new_pay.search_index(search).payment.getConcept());
            textObservation.setText(new_pay.search_index(search).payment.getObservations());
            textValue.setText(String.valueOf(new_pay.search_index(search).payment.getValue()));
            clean_table();
            model_list.addRow(new Object[]{textConcept.getText(), textObservation.getText(), Integer.parseInt(textValue.getText()), new_pay.search_index(search).payment.getDate(), Boolean.valueOf(checkBoxRefunded.getText())});
          }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the data");
      } 
    }
    
    public void table() {

        ConexionSQL conexion = new ConexionSQL();

        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {

                Object id = jTablePayments.getValueAt(row, 0);
                Object first = jTablePayments.getValueAt(row, 1);
                Object second = jTablePayments.getValueAt(row, 2);
                Object third = jTablePayments.getValueAt(row, 3);
                Object fourth = jTablePayments.getValueAt(row, 4);
                Object fifth = jTablePayments.getValueAt(row, 5);
                Object sixth = jTextFieldEmployeeId.getText();

                String[] fields = new String[6];

                fields[0] = first.toString();
                fields[1] = second.toString();
                fields[2] = third.toString();
                fields[3] = fourth.toString();
                fields[4] = fifth.toString();
                fields[5] = sixth.toString();

                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.updateRow(tableName, id.toString(), fields);
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
                return true;
            }
        };

        model.addColumn("id");
        model.addColumn("Concept");
        model.addColumn("Value");
        model.addColumn("Payment date");
        model.addColumn("Observations");
        model.addColumn("Refunded");

        this.jTablePayments.setModel(model);
        jTablePayments.getColumnModel().getColumn(0).setMinWidth(0);
        jTablePayments.getColumnModel().getColumn(0).setMaxWidth(0);
        jTablePayments.getColumnModel().getColumn(1).setPreferredWidth(105);
        jTablePayments.getColumnModel().getColumn(2).setPreferredWidth(105);
        jTablePayments.getColumnModel().getColumn(3).setPreferredWidth(105);
        jTablePayments.getColumnModel().getColumn(4).setPreferredWidth(120);
        jTablePayments.getColumnModel().getColumn(5).setPreferredWidth(120);

        int employee_id = Integer.parseInt(jTextFieldEmployeeId.getText());
        String[][] result = conexion.tableRelation(tableName, employee_id);

        for (int i = 0; i < result.length; i++) {
            model.addRow(result[i]);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        btn_back_payments = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        text_payment_search = new javax.swing.JTextField();
        btn_search_payments = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        textConcept = new javax.swing.JTextField();
        textObservation = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        paymentDate = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        textValue = new javax.swing.JTextField();
        checkBoxRefunded = new javax.swing.JCheckBox();
        btn_create_payment = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnConfirm = new javax.swing.JButton();
        jTextFieldId = new javax.swing.JTextField();
        jTextFieldEmployeeId = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePayments = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_pay = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N

        btn_back_payments.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back_payments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_back_paymentsActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabel1.setText("                                               Payments");

        jLabel2.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabel2.setText(" Seach: ");

        text_payment_search.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)));
        text_payment_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                text_payment_searchActionPerformed(evt);
            }
        });

        btn_search_payments.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        btn_search_payments.setMaximumSize(new java.awt.Dimension(42, 42));
        btn_search_payments.setMinimumSize(new java.awt.Dimension(42, 42));
        btn_search_payments.setPreferredSize(new java.awt.Dimension(42, 42));
        btn_search_payments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search_paymentsActionPerformed(evt);
            }
        });

        jLabel3.setText(" Concept: ");

        jLabel4.setText(" Observation: ");

        textConcept.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)));

        textObservation.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204)));

        jLabel5.setText(" Date:");

        jLabel6.setText(" Value:");

        checkBoxRefunded.setText("   Refunded");

        btn_create_payment.setText("Create");
        btn_create_payment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_create_paymentActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        jTablePayments.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTablePayments);

        jScrollPane2.setViewportView(table_pay);

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                        .addComponent(btn_back_payments)
                        .addGap(0, 0, 0)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(text_payment_search, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_search_payments, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 234, Short.MAX_VALUE))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(0, 0, 0)
                                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(textConcept, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                                            .addComponent(textObservation))
                                        .addGap(18, 18, 18)
                                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(panelImage1Layout.createSequentialGroup()
                                                .addGap(62, 62, 62)
                                                .addComponent(checkBoxRefunded))
                                            .addGroup(panelImage1Layout.createSequentialGroup()
                                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(textValue, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(panelImage1Layout.createSequentialGroup()
                                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(paymentDate, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                                        .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(jTextFieldEmployeeId, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addComponent(btn_create_payment, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnUpdate)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnDelete)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnConfirm)))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 627, Short.MAX_VALUE)
                            .addComponent(jScrollPane1))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_back_payments)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                        .addComponent(btn_search_payments, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(text_payment_search, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldEmployeeId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(28, 28, 28)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textConcept, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textObservation, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_create_payment)
                            .addComponent(btnUpdate)
                            .addComponent(btnDelete)
                            .addComponent(btnConfirm))
                        .addGap(8, 8, 8))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(paymentDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(checkBoxRefunded)))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_back_paymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_back_paymentsActionPerformed
        Employees obj = new Employees();
        obj.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_back_paymentsActionPerformed

    private void btn_create_paymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_create_paymentActionPerformed
        add_elements();
        String concept = textConcept.getText();
        String value = textValue.getText();
        String paymentDate = String.valueOf(this.paymentDate.getDate());
        String obervations = textObservation.getText();
        String refunded = "";
        
        if (checkBoxRefunded.isSelected()) {
            refunded = "1";
        } else {
            refunded = "0";
        }
        String employeeId = jTextFieldEmployeeId.getText();

        if (concept.isEmpty() || value.isEmpty() || paymentDate.length() <= 4 || obervations.isEmpty()){
            JOptionPane.showMessageDialog(null, "Todos lo campos son requeridos");
        } else {
            String[] fields = new String[6];

            fields[0] = concept;
            fields[1] = value;
            fields[2] = paymentDate;
            fields[3] = obervations;
            fields[4] = refunded;
            fields[5] = employeeId;

            ConexionSQL conexion = new ConexionSQL();
            conexion.newRegister(tableName, fields);
            table();
        }
    }//GEN-LAST:event_btn_create_paymentActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedRow = jTablePayments.getSelectedRow();
        if (selectedRow >= 0) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure ?");
            if (dialogResult == JOptionPane.YES_OPTION) {
                Object id = jTablePayments.getValueAt(selectedRow, 0);
                model.removeRow(selectedRow);
                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.removeRow(tableName, id.toString());
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
            }
        }
        
         delete();
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        update();
        
        int selectedRow = jTablePayments.getSelectedRow();

        if (selectedRow >= 0) {
            int col = 0;
            Object id = jTablePayments.getValueAt(selectedRow, col);
            Object concept = jTablePayments.getValueAt(selectedRow, col += 1);
            Object value = jTablePayments.getValueAt(selectedRow, col += 1);
            Object paymentDate = jTablePayments.getValueAt(selectedRow, col += 1);
            Object observations = jTablePayments.getValueAt(selectedRow, col += 1);
            Object refunded = jTablePayments.getValueAt(selectedRow, col += 1);

            try {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                Date dateOriginal = formatter.parse(paymentDate.toString());

                jTextFieldId.setText(id.toString());
                textConcept.setText(concept.toString());
                textValue.setText(value.toString());
                this.paymentDate.setDate(dateOriginal);
                textObservation.setText(observations.toString());
                if (refunded.toString().equals("1")) {
                    checkBoxRefunded.setSelected(true);
                } else {
                    checkBoxRefunded.setSelected(false);
                }
            } catch (Exception e) {
                System.out.println("unsuccessfully" + e);
            }

        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        // TODO add your handling code here:
        String[] fields = new String[6];

        String id = jTextFieldId.getText();
        String concept = textConcept.getText();
        String value = textValue.getText();
        String paymentDate = String.valueOf(this.paymentDate.getDate());
        String observations = textObservation.getText();
        String refunded = "";
        if (checkBoxRefunded.isSelected()) {
            refunded = "1";
        } else {
            refunded = "0";
        }
        String empoyee_id = jTextFieldEmployeeId.getText();

        fields[0] = concept;
        fields[1] = value;
        fields[2] = paymentDate;
        fields[3] = observations;
        fields[4] = refunded;
        fields[5] = empoyee_id;
        try {
            ConexionSQL conexion = new ConexionSQL();
            conexion.updateRow(tableName, id, fields);
        } catch (Exception e) {
            System.out.println("connection error:" + e);
        }
        table();
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void text_payment_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_text_payment_searchActionPerformed
    }//GEN-LAST:event_text_payment_searchActionPerformed

    private void btn_search_paymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_paymentsActionPerformed
      if(text_payment_search.getText().length() > 0){
         if(!new_pay.is_empty()){
           search_elements(text_payment_search.getText());   
         }
         else{
           JOptionPane.showMessageDialog(null, "List of Cvs are empty");
         }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the data for search.");
      }
    }//GEN-LAST:event_btn_search_paymentsActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Payments_employee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Payments_employee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Payments_employee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Payments_employee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
               new Payments_employee().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btn_back_payments;
    private javax.swing.JButton btn_create_payment;
    private javax.swing.JButton btn_search_payments;
    private javax.swing.JCheckBox checkBoxRefunded;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTablePayments;
    public javax.swing.JTextField jTextFieldEmployeeId;
    private javax.swing.JTextField jTextFieldId;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private com.toedter.calendar.JDateChooser paymentDate;
    private javax.swing.JTable table_pay;
    public javax.swing.JTextField textConcept;
    private javax.swing.JTextField textObservation;
    private javax.swing.JTextField textValue;
    public javax.swing.JTextField text_payment_search;
    // End of variables declaration//GEN-END:variables
}
