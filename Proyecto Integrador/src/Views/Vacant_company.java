package Views;
import Lists.Simple_list_vacant;
import Classes.Vacant;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author libardo
 */
public class Vacant_company extends javax.swing.JFrame{
    DefaultTableModel model;

    public Vacant_company(){
      initComponents();
      this.setTitle("Vacants");
      this.setLocationRelativeTo(this);
      
      model = new DefaultTableModel(){
      @Override
        public boolean isCellEditable(int row, int column){
          Object first = table_vacant.getValueAt(row, 0);
          Object second = table_vacant.getValueAt(row, 1);
          Object third = table_vacant.getValueAt(row, 2);
          Object fourth = table_vacant.getValueAt(row, 3);

          String[] fields = new String[4];

          fields[0] = first.toString();
          fields[1] = second.toString();
          fields[2] = third.toString();
          fields[3] = fourth.toString();

          return true;
        }
      };
    
      model.addColumn("Name");
      model.addColumn("Description");
      model.addColumn("Position");
      model.addColumn("Area");

      this.table_vacant.setModel(model);
      table_vacant.getColumnModel().getColumn(0).setPreferredWidth(105);
      table_vacant.getColumnModel().getColumn(1).setPreferredWidth(105);
      table_vacant.getColumnModel().getColumn(2).setPreferredWidth(105);
      table_vacant.getColumnModel().getColumn(3).setPreferredWidth(120);
    }
    
    Simple_list_vacant new_vacant = new Simple_list_vacant();
    
    public void clear(){
      text_search_vacant.setText(null);
      text_name_vacant.setText(null);
      text_position_vacant.setText(null);
      text_description_vacant.setText(null);
      text_area_vacant.setText(null);
    }
    
    public void clean_table(){
      try{
         int rows = table_vacant.getRowCount();
          if (rows > 0){
            for(int i = 0; rows > i; i++){
               model.removeRow(0);
            }
          }
      }
      catch(Exception e){
        JOptionPane.showMessageDialog(null, "Failed to clean the table.");
      }
    }
    
    public void add_elements(){
      if(text_name_vacant.getText().length() > 0 && text_position_vacant.getText().length() > 0 && text_description_vacant.getText().length() > 0 && text_area_vacant.getText().length() > 0){
        Vacant vacant = new Vacant(text_name_vacant.getText(), text_position_vacant.getText(), text_description_vacant.getText(), text_area_vacant.getText());
        new_vacant.insert(vacant);
        if(!new_vacant.is_empty()){
          clean_table();
          for(int i = 0; i < new_vacant.size(); i++){
            model.addRow(new Object[]{new_vacant.display(i).vacant.getName(), new_vacant.display(i).vacant.getDescription(), new_vacant.display(i).vacant.getPosition(), new_vacant.display(i).vacant.getArea()});
          } 
        }
        clear();
        JOptionPane.showMessageDialog(null, "Create successfully");  
      }
      else{
        JOptionPane.showMessageDialog(null, "The data are required for create.");
      }
    }
    
    public void delete(){
      new_vacant.delete_list(text_name_vacant.getText(), text_position_vacant.getText(), text_description_vacant.getText(), text_area_vacant.getText());
      clear();
      clean_table();
       if(!new_vacant.is_empty()){
         for(int i = 0; i < new_vacant.size(); i++){
           model.addRow(new Object[]{new_vacant.display(i).vacant.getName(), new_vacant.display(i).vacant.getDescription(), new_vacant.display(i).vacant.getPosition(), new_vacant.display(i).vacant.getArea()});
         }   
       }
    }
    
    private static boolean is_numeric(String data){
     try{
        Integer.parseInt(data);
        return true;
     }
     catch(NumberFormatException nfe){
       return false;
     }
    }
    
    public void search_elements(String search){ 
      if(search != null){
        if(is_numeric(search)){
          JOptionPane.showMessageDialog(null, "Don't enter numbers.");
        }
        else{
          if(new_vacant.is_empty()){
            JOptionPane.showMessageDialog(null, "The size of list is:" + new_vacant.size() );
          }
          else{
            text_name_vacant.setText(new_vacant.search_index(search).vacant.getName());
            text_position_vacant.setText(new_vacant.search_index(search).vacant.getPosition());
            text_description_vacant.setText(new_vacant.search_index(search).vacant.getDescription());
            text_area_vacant.setText(new_vacant.search_index(search).vacant.getArea());
            clean_table();
            model.addRow(new Object[]{text_name_vacant.getText(), text_description_vacant.getText(), text_position_vacant.getText(), text_area_vacant.getText()} );
          }
        }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the data");
      } 
    }

    public void update(){
      if(!new_vacant.is_empty()){
        if(text_search_vacant.getText().length() > 0){
           Vacant vacant = new Vacant(text_name_vacant.getText(), text_position_vacant.getText(), text_description_vacant.getText(), text_area_vacant.getText());
           new_vacant.update(text_search_vacant.getText(), vacant);
            clean_table();
            for(int i = 0; i < new_vacant.size(); i++){
              model.addRow(new Object[]{new_vacant.display(i).vacant.getName(), new_vacant.display(i).vacant.getDescription(), new_vacant.display(i).vacant.getPosition(), new_vacant.display(i).vacant.getArea()});
            } 
           clear();
           JOptionPane.showMessageDialog(null, "Updated successfully.");
        }
        else{
          JOptionPane.showMessageDialog(null, "Search the vacant to update.");
        }   
      }
      else{
          JOptionPane.showMessageDialog(null, "The list is empty.");
      }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        btn_back_vacant = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        text_search_vacant = new javax.swing.JTextField();
        btn_search_vacant = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        text_name_vacant = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        text_position_vacant = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        text_description_vacant = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        text_area_vacant = new javax.swing.JTextField();
        btn_create_vancant = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        table_vacant = new javax.swing.JTable();
        btn_update_vacant = new javax.swing.JButton();
        btn_delete_vacant = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(650, 490));
        setSize(new java.awt.Dimension(650, 490));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N
        panelImage1.setPreferredSize(new java.awt.Dimension(650, 490));

        btn_back_vacant.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back_vacant.setSize(new java.awt.Dimension(49, 42));
        btn_back_vacant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_back_vacantActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabel1.setText("                                                   Vacants");

        jLabel2.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 14)); // NOI18N
        jLabel2.setText(" Search:");

        btn_search_vacant.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        btn_search_vacant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search_vacantActionPerformed(evt);
            }
        });

        jLabel3.setText(" Name:");

        jLabel4.setText(" Position: ");

        jLabel5.setText(" Description: ");

        text_description_vacant.setColumns(20);
        text_description_vacant.setRows(5);
        jScrollPane1.setViewportView(text_description_vacant);

        jLabel6.setText(" Area: ");

        btn_create_vancant.setText("Create");
        btn_create_vancant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_create_vancantActionPerformed(evt);
            }
        });

        jScrollPane3.setViewportView(table_vacant);

        btn_update_vacant.setText("Update");
        btn_update_vacant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_update_vacantActionPerformed(evt);
            }
        });

        btn_delete_vacant.setText("Delete");
        btn_delete_vacant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_delete_vacantActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addComponent(btn_back_vacant)
                .addGap(0, 0, 0)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(btn_create_vancant, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(btn_update_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_delete_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                            .addComponent(text_name_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(text_position_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(panelImage1Layout.createSequentialGroup()
                            .addComponent(text_search_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, 0)
                            .addComponent(btn_search_vacant))
                        .addGroup(panelImage1Layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(text_area_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 30, Short.MAX_VALUE))
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_back_vacant, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 2, Short.MAX_VALUE))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_search_vacant))
                    .addComponent(text_search_vacant, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(text_name_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(text_position_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(text_area_vacant, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_create_vancant)
                    .addComponent(btn_update_vacant)
                    .addComponent(btn_delete_vacant))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(103, 103, 103))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_create_vancantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_create_vancantActionPerformed
      add_elements();
    }//GEN-LAST:event_btn_create_vancantActionPerformed

    private void btn_search_vacantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_vacantActionPerformed
      if(text_search_vacant.getText().length() > 0){
         if(!new_vacant.is_empty()){
           search_elements(text_search_vacant.getText());   
         }
         else{
           JOptionPane.showMessageDialog(null, "List of Vacants are empty");
         }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the data for search.");
      }
    }//GEN-LAST:event_btn_search_vacantActionPerformed

    private void btn_update_vacantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_update_vacantActionPerformed
      update();       
    }//GEN-LAST:event_btn_update_vacantActionPerformed

    private void btn_delete_vacantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_delete_vacantActionPerformed
      delete();
    }//GEN-LAST:event_btn_delete_vacantActionPerformed

    private void btn_back_vacantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_back_vacantActionPerformed
        this.dispose();
        new Selection_program().setVisible(true);
    }//GEN-LAST:event_btn_back_vacantActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vacant_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vacant_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vacant_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vacant_company.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
              new Vacant_company().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back_vacant;
    private javax.swing.JButton btn_create_vancant;
    private javax.swing.JButton btn_delete_vacant;
    private javax.swing.JButton btn_search_vacant;
    private javax.swing.JButton btn_update_vacant;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JTable table_vacant;
    private javax.swing.JTextField text_area_vacant;
    private javax.swing.JTextArea text_description_vacant;
    private javax.swing.JTextField text_name_vacant;
    private javax.swing.JTextField text_position_vacant;
    private javax.swing.JTextField text_search_vacant;
    // End of variables declaration//GEN-END:variables
}
