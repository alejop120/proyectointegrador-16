package Views;
import Classes.ConexionSQL;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * @author libardo
 */
public class Endowment extends javax.swing.JFrame{
    Vector<Object> Delivered_selection;
    String tableName = "endowment";
    DefaultTableModel model;
    
    public Endowment(){
      initComponents();
      jTextFieldId.setVisible(false);
      jTextFieldContractId.setVisible(false);
      Delivered_selection = new Vector<Object>();
      this.setTitle("Endowment");
      this.setLocationRelativeTo(this);
      selection();
    }
    
    public void table() {

        ConexionSQL conexion = new ConexionSQL();

        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {

                Object id = jTableEndowments.getValueAt(row, 0);
                Object first = jTableEndowments.getValueAt(row, 1);
                Object second = jTableEndowments.getValueAt(row, 2);
                Object third = jTableEndowments.getValueAt(row, 3);

                String[] fields = new String[3];

                fields[0] = first.toString();
                fields[1] = second.toString();
                fields[2] = third.toString();

                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.updateRow(tableName, id.toString(), fields);
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
                return true;
            }
        };
        model.addColumn("id");
        model.addColumn("Name");
        model.addColumn("Size");
        model.addColumn("Delivered");

        this.jTableEndowments.setModel(model);
        jTableEndowments.getColumnModel().getColumn(0).setMinWidth(0);
        jTableEndowments.getColumnModel().getColumn(0).setMaxWidth(0);
        jTableEndowments.getColumnModel().getColumn(1).setPreferredWidth(105);
        jTableEndowments.getColumnModel().getColumn(2).setPreferredWidth(105);
        jTableEndowments.getColumnModel().getColumn(3).setPreferredWidth(105);

        int contract_id = Integer.parseInt(jTextFieldContractId.getText());
        String[][] result = conexion.tableRelation(tableName, contract_id);

        for (int i = 0; i < result.length; i++) {
            model.addRow(result[i]);
        }
    }
    
    public void selection(){
//        Delivered_selection.add("Yes");
//        Delivered_selection.add("No");
//        for (int i = 0; i < Delivered_selection.size(); i++) {
//            selection_delivered_endowment.addItem(Delivered_selection.get(i).toString());
//        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        btn_back_endowment = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textSize = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btnCreate = new javax.swing.JButton();
        jCheckBoxDelivered = new javax.swing.JCheckBox();
        jButtonUpdate = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();
        jButtonConfirm = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableEndowments = new javax.swing.JTable();
        jTextFieldId = new javax.swing.JTextField();
        jTextFieldContractId = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 390));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N

        btn_back_endowment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back_endowment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_back_endowmentActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Endowment");

        jLabel2.setText(" Name: ");

        jLabel3.setText(" Size: ");

        jLabel4.setText(" Delivered:");

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        jCheckBoxDelivered.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxDeliveredActionPerformed(evt);
            }
        });

        jButtonUpdate.setText("Update");
        jButtonUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateActionPerformed(evt);
            }
        });

        jButtonDelete.setText("Delete");
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        jButtonConfirm.setText("Confirm");
        jButtonConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConfirmActionPerformed(evt);
            }
        });

        jTableEndowments.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableEndowments);

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButtonDelete, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
                            .addComponent(btnCreate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonConfirm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonUpdate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(textSize)
                    .addComponent(jCheckBoxDelivered)
                    .addComponent(textName))
                .addGap(29, 29, 29)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldContractId, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(btn_back_endowment, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_back_endowment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(textName, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                        .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(textSize, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addComponent(jTextFieldContractId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxDelivered))
                .addGap(18, 18, 18)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreate)
                    .addComponent(jButtonUpdate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonConfirm)
                    .addComponent(jButtonDelete))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_back_endowmentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_back_endowmentActionPerformed
        try {
            ConexionSQL conexionSql = new ConexionSQL();
            String consult = "select * from contract WHERE contract_id="+ jTextFieldContractId.getText();
            Statement statement = conexionSql.conexion.createStatement();
            ResultSet result = statement.executeQuery(consult);
            
            String employeeId = "";
            while (result.next()) {
                employeeId = result.getString("employee_id");
            }
            Contracts obj = new Contracts();
            obj.jTextFieldEmployeeId.setText(employeeId);
            obj.table();
            obj.setVisible(true);
            dispose();
        } catch (Exception e) {
            System.out.println("Unsuccessfully "+ e);
        }
    }//GEN-LAST:event_btn_back_endowmentActionPerformed

    private void jCheckBoxDeliveredActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxDeliveredActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBoxDeliveredActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        // TODO add your handling code here:
        String name = textName.getText();
        String size = textSize.getText();
        String delivered = "";
        
        if(jCheckBoxDelivered.isSelected()){
          delivered = "1";
        }else{
          delivered = "0";
        }

        if (name.isEmpty()|| size.isEmpty()) {
            System.out.println("Campos obligatorios");
            JOptionPane.showMessageDialog(null, "Todos lo campos son requeridos");
        } else {
            String[] fields = new String[4];

            fields[0] = name;
            fields[1] = size;
            fields[2] = delivered;
            fields[3] = jTextFieldContractId.getText();
            
            ConexionSQL conexion = new ConexionSQL();
            conexion.newRegister(tableName, fields);
            table();
        }
    }//GEN-LAST:event_btnCreateActionPerformed

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        // TODO add your handling code here:
        int selectedRow = jTableEndowments.getSelectedRow();
        if (selectedRow >= 0) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure ?");
            if (dialogResult == JOptionPane.YES_OPTION) {
                Object id = jTableEndowments.getValueAt(selectedRow, 0);
                model.removeRow(selectedRow);
                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.removeRow(tableName, id.toString());
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
            }
        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateActionPerformed
        // TODO add your handling code here:

        int selectedRow = jTableEndowments.getSelectedRow();
        if (selectedRow >= 0) {
            int col = 0;
            Object id = jTableEndowments.getValueAt(selectedRow, col);
            Object name = jTableEndowments.getValueAt(selectedRow, col+=1);
            Object size = jTableEndowments.getValueAt(selectedRow, col+=1);
            Object delivered = jTableEndowments.getValueAt(selectedRow, col+=1);
            
            try {
                jTextFieldId.setText(id.toString());
                textName.setText(name.toString());
                textSize.setText(size.toString());
                if(delivered.toString().equals("1")){
                    jCheckBoxDelivered.setSelected(true);
                }else{
                    jCheckBoxDelivered.setSelected(false);
                }
            } catch (Exception e) {
                System.out.println("unsuccessfully" + e);
            }
        }
    }//GEN-LAST:event_jButtonUpdateActionPerformed

    private void jButtonConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConfirmActionPerformed
        // TODO add your handling code here:
        String id = jTextFieldId.getText();
        String name = textName.getText();
        String size = textSize.getText();
        String delivered = "";
        if(jCheckBoxDelivered.isSelected()){
          delivered = "1";
        }else{
          delivered = "0";
        }
        String contract_id = jTextFieldContractId.getText();

        String[] fields = new String[4];
        
        fields[0] = name;
        fields[1] = size;
        fields[2] = delivered;
        fields[3] = contract_id;

        
        try {
            ConexionSQL conexion = new ConexionSQL();
            conexion.updateRow(tableName, id, fields);
        } catch (Exception e) {
            System.out.println("connection error:" + e);
        }
        table();
    }//GEN-LAST:event_jButtonConfirmActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Endowment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Endowment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Endowment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Endowment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
              new Endowment().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btn_back_endowment;
    private javax.swing.JButton jButtonConfirm;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonUpdate;
    private javax.swing.JCheckBox jCheckBoxDelivered;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableEndowments;
    public javax.swing.JTextField jTextFieldContractId;
    private javax.swing.JTextField jTextFieldId;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JTextField textName;
    private javax.swing.JTextField textSize;
    // End of variables declaration//GEN-END:variables
}
