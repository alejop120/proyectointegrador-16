package Views;
import Lists.Simple_list;
import Classes.User;
import java.awt.Component;
import java.util.Vector;
import javax.swing.JOptionPane;
/**
 * @author libardo sánchez
 */
public class User_list_simple extends javax.swing.JFrame {
    Vector<Object>List_role;
    Vector<Object>list_Nodo;
    public void setLocationRelativeto(Component c){
      super.setLocationRelativeTo(c);
    }
 
    public User_list_simple() {
      initComponents();
      this.setTitle("Users");
      List_role =new Vector<Object>();
      list_Nodo =new Vector<Object>();
      selection();
      list_elements();
      list_methods();
      setLocationRelativeTo(this);
    }
    
    Simple_list new_list = new Simple_list();
    
    public void selection(){
      List_role.add("Male");
      List_role.add("Femenine");
      for(int i = 0 ; i<List_role.size() ; i++){
        select_role_simple.addItem(List_role.get(i).toString());
      }  
    }
    
    public void add_elements(){
      User user = new User(text_name_simple.getText(), text_surname_simple.getText(), text_document_simple.getText(), select_role_simple.getSelectedItem().toString());
      new_list.insert(user);
      clear();
    }

    public void clear(){
      text_name_simple.setText(null);
      text_document_simple.setText(null);
      text_surname_simple.setText(null);
      birthdate_simple.setDateFormatString(null);
    }
    
    public void list_elements() {
      jTextArea1.setText(new_list.show());
    }
    
    public void list_methods() {
       list_Nodo.add("Insert at the beginning");
       list_Nodo.add("Insert last");
       list_Nodo.add("Delete at the beginning");
       list_Nodo.add("Delete last");
       list_Nodo.add("Insert between users");
       list_Nodo.add("Delete between users");
       for (int i = 0; i < list_Nodo.size(); i++) {
         selection_operation_simple.addItem(list_Nodo.get(i).toString());
       }
    }

    public void delete(){
      new_list.delete_list();
    }
    
    public void operation(){
        if(selection_operation_simple.getSelectedItem().equals("Delete at the beginning")){
          new_list.delete_initial();
        }

        if(selection_operation_simple.getSelectedItem().equals("Insert at the beginning")){
          User user_new = new User(text_name_simple.getText(), text_surname_simple.getText(), text_document_simple.getText(), select_role_simple.getSelectedItem().toString());
          new_list.insert_initial(user_new);
          clear();
        }

        if(selection_operation_simple.getSelectedItem().equals("Insert last")){
          add_elements();
        }

        if(selection_operation_simple.getSelectedItem().equals("Delete last")){
          new_list.delete_last();
        }

        if(selection_operation_simple.getSelectedItem().equals("Insert between users")){
          int position = Integer.parseInt(JOptionPane.showInputDialog("Please enter the position you want to insert"));
          User user_new = new User(text_name_simple.getText(), text_surname_simple.getText(), text_document_simple.getText(), select_role_simple.getSelectedItem().toString());
          new_list.insert_between_node(user_new, position);
          clear();
        }

        if(selection_operation_simple.getSelectedItem().equals("Delete between users")){
          int position = Integer.parseInt(JOptionPane.showInputDialog("Please choose the position you want to delete"));
          new_list.delete_between_node(position);
        }
    }
    
    public void search_elements(){
      String search = JOptionPane.showInputDialog("Enter the position of the user to search: ");
      if(search != null){
        int respond = Integer.parseInt(search) - 1;
        if(respond >= 0 && respond <= new_list.size()){
         JOptionPane.showMessageDialog(null, "The data are found:\n" + new_list.search_index(respond).data);
         jTextArea1.setText(new_list.search_index(respond).data.toString());
        }
        else{
         JOptionPane.showMessageDialog(null, "Enter the correct position\n The size of list is:" +new_list.size() );
        }
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the position");
      }
    }

    public void update(){
      int position = Integer.parseInt(JOptionPane.showInputDialog("Enter the position of the user you want to replace"));
      if(position <= new_list.size()){
        User user = new User(text_name_simple.getText(), text_surname_simple.getText(), text_document_simple.getText(), select_role_simple.getSelectedItem().toString());
        new_list.change(position, user);
        clear();
      }
      else{
        JOptionPane.showMessageDialog(null, "Enter the correct position\n the size of list is:" + new_list.size());
      }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        text_name_simple = new javax.swing.JTextField();
        text_surname_simple = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        select_role_simple = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        text_document_simple = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        btn_create_simple = new javax.swing.JButton();
        btn_update_simple = new javax.swing.JButton();
        btn_search_simple = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        selection_operation_simple = new javax.swing.JComboBox<>();
        btn_create_operation_simple = new javax.swing.JButton();
        btn_delete_simple = new javax.swing.JButton();
        btn_show_simple = new javax.swing.JButton();
        btn_clean_simple = new javax.swing.JButton();
        btn_back_users = new javax.swing.JButton();
        birthdate_simple = new com.toedter.calendar.JDateChooser();

        jToggleButton1.setText("jToggleButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Users");

        jLabel2.setText(" Name:");

        jLabel3.setText(" Surname:");

        text_name_simple.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(240, 236, 236), 1, true));

        text_surname_simple.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(234, 232, 232), 1, true));

        jLabel4.setText("Age:");

        jLabel5.setText("Role: ");

        jLabel6.setText(" Document:");

        text_document_simple.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(233, 232, 232), 1, true));

        jScrollPane1.setBackground(new java.awt.Color(204, 204, 204));
        jScrollPane1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 3, true));

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        btn_create_simple.setText("Create");
        btn_create_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_create_simpleActionPerformed(evt);
            }
        });

        btn_update_simple.setText("Update");
        btn_update_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_update_simpleActionPerformed(evt);
            }
        });

        btn_search_simple.setText("Search");
        btn_search_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search_simpleActionPerformed(evt);
            }
        });

        jLabel7.setText("Operation:");

        selection_operation_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selection_operation_simpleActionPerformed(evt);
            }
        });

        btn_create_operation_simple.setText("Accept");
        btn_create_operation_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_create_operation_simpleActionPerformed(evt);
            }
        });

        btn_delete_simple.setText("Delete");
        btn_delete_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_delete_simpleActionPerformed(evt);
            }
        });

        btn_show_simple.setText("Show");
        btn_show_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_show_simpleActionPerformed(evt);
            }
        });

        btn_clean_simple.setText("Clean");
        btn_clean_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_clean_simpleActionPerformed(evt);
            }
        });

        btn_back_users.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back_users.setMaximumSize(new java.awt.Dimension(49, 42));
        btn_back_users.setMinimumSize(new java.awt.Dimension(49, 42));
        btn_back_users.setPreferredSize(new java.awt.Dimension(49, 42));
        btn_back_users.setSize(new java.awt.Dimension(49, 42));
        btn_back_users.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_back_usersActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                .addComponent(btn_back_users, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 506, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addComponent(btn_create_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_update_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_search_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(text_document_simple, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                                    .addComponent(text_name_simple)
                                    .addComponent(text_surname_simple))
                                .addGap(18, 18, 18)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addComponent(btn_delete_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_show_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_clean_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btn_create_operation_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(selection_operation_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 57, Short.MAX_VALUE))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(select_role_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(birthdate_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(57, Short.MAX_VALUE))))))
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_back_users, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(birthdate_simple, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(text_surname_simple, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(select_role_simple, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(text_document_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(selection_operation_simple, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(text_name_simple, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE)
                        .addGap(78, 78, 78)))
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_create_operation_simple)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_create_simple)
                            .addComponent(btn_update_simple)
                            .addComponent(btn_search_simple))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_delete_simple)
                            .addComponent(btn_show_simple)
                            .addComponent(btn_clean_simple))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_search_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_simpleActionPerformed
      search_elements();
    }//GEN-LAST:event_btn_search_simpleActionPerformed

    private void selection_operation_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selection_operation_simpleActionPerformed

    }//GEN-LAST:event_selection_operation_simpleActionPerformed

    private void btn_show_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_show_simpleActionPerformed
      list_elements();
    }//GEN-LAST:event_btn_show_simpleActionPerformed

    private void btn_create_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_create_simpleActionPerformed
      add_elements();
      JOptionPane.showMessageDialog(this, "Creado correctamente.");
    }//GEN-LAST:event_btn_create_simpleActionPerformed

    private void btn_delete_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_delete_simpleActionPerformed
      delete();
    }//GEN-LAST:event_btn_delete_simpleActionPerformed

    private void btn_create_operation_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_create_operation_simpleActionPerformed
      operation();
      JOptionPane.showMessageDialog(this, "La operación se ejecuto con éxito.");
    }//GEN-LAST:event_btn_create_operation_simpleActionPerformed

    private void btn_clean_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_clean_simpleActionPerformed
      clear();
    }//GEN-LAST:event_btn_clean_simpleActionPerformed

    private void btn_update_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_update_simpleActionPerformed
      update();
    }//GEN-LAST:event_btn_update_simpleActionPerformed

    private void btn_back_usersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_back_usersActionPerformed
      this.dispose();
      Selection_program selection = new Selection_program();
      selection.setVisible(true);
    }//GEN-LAST:event_btn_back_usersActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(User_list_simple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(User_list_simple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(User_list_simple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(User_list_simple.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new User_list_simple().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser birthdate_simple;
    private javax.swing.JButton btn_back_users;
    private javax.swing.JButton btn_clean_simple;
    private javax.swing.JButton btn_create_operation_simple;
    private javax.swing.JButton btn_create_simple;
    private javax.swing.JButton btn_delete_simple;
    private javax.swing.JButton btn_search_simple;
    private javax.swing.JButton btn_show_simple;
    private javax.swing.JButton btn_update_simple;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JToggleButton jToggleButton1;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JComboBox<String> select_role_simple;
    private javax.swing.JComboBox<String> selection_operation_simple;
    private javax.swing.JTextField text_document_simple;
    private javax.swing.JTextField text_name_simple;
    private javax.swing.JTextField text_surname_simple;
    // End of variables declaration//GEN-END:variables
}
