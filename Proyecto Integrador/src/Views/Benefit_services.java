package Views;
import Classes.Benefit_service;
import Classes.ConexionSQL;
import Lists.Simple_list_benefit_service;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author libardo
 */
public class Benefit_services extends javax.swing.JFrame{
    
    DefaultTableModel model;
    DefaultTableModel model_list;
    String tableName = "benefit_service";
    public Benefit_services(){
      initComponents();
      jTextFieldId.setVisible(false);
      jTextFieldEmployeeId.setVisible(false);
      this.setTitle("Benefit Service");
      this.setLocationRelativeTo(this);
      
      model_list = new DefaultTableModel(){
      @Override
        public boolean isCellEditable(int row, int column){
          Object first = table_benefit.getValueAt(row, 0);
          Object second = table_benefit.getValueAt(row, 1);
          Object third = table_benefit.getValueAt(row, 2);
          Object fourth = table_benefit.getValueAt(row, 3);

          String[] fields = new String[4];

          fields[0] = first.toString();
          fields[1] = second.toString();
          fields[2] = third.toString();
          fields[3] = fourth.toString();

          return true;
        }
      };
    
      model_list.addColumn("Arl");
      model_list.addColumn("Eps");
      model_list.addColumn("Pension severance");
      model_list.addColumn("Compensation fund");

      this.table_benefit.setModel(model_list);
      table_benefit.getColumnModel().getColumn(0).setPreferredWidth(105);
      table_benefit.getColumnModel().getColumn(1).setPreferredWidth(105);
      table_benefit.getColumnModel().getColumn(2).setPreferredWidth(105);
      table_benefit.getColumnModel().getColumn(3).setPreferredWidth(120);
      
    }
    
    Simple_list_benefit_service new_benefit = new Simple_list_benefit_service();
    
    public void clear(){
      textArl.setText(null);
      textEps.setText(null);
      textPension.setText(null);
      textCompenssation.setText(null);
    }
    
    public void clean_table(){
      try{
         int rows = table_benefit.getRowCount();
          if (rows > 0){
            for(int i = 0; rows > i; i++){
               model_list.removeRow(0);
            }
          }
      }
      catch(Exception e){
        JOptionPane.showMessageDialog(null, "Failed to clean the table.");
      }
    }
    
    public void add_elements(){
      if(textArl.getText().length() > 0 && textEps.getText().length() > 0 && textPension.getText().length() > 0 && textCompenssation.getText().length() > 0){
        Benefit_service benefit = new Benefit_service(textArl.getText(), textEps.getText(), textPension.getText(), textCompenssation.getText());
        new_benefit.insert(benefit);
        if(!new_benefit.is_empty()){
          clean_table();
          for(int i = 0; i < new_benefit.size(); i++){
            model_list.addRow(new Object[]{new_benefit.display(i).benefit.getArl(), new_benefit.display(i).benefit.getEps(), new_benefit.display(i).benefit.getPension_severance(), new_benefit.display(i).benefit.getCompensation_fund()});
          } 
        }
        clear();
        JOptionPane.showMessageDialog(null, "Create successfully");  
      }
      else{
        JOptionPane.showMessageDialog(null, "The data are required for create.");
      }
    }
    
    public void delete(){
      if(textArl.getText().length() > 0 && textEps.getText().length() > 0 && textPension.getText().length() > 0 && textCompenssation.getText().length() > 0){
       new_benefit.delete_list(textArl.getText(), textEps.getText(), textPension.getText(), textCompenssation.getText());
       clear();
       clean_table();
        if(!new_benefit.is_empty()){
          for(int i = 0; i < new_benefit.size(); i++){
            model_list.addRow(new Object[]{new_benefit.display(i).benefit.getArl(), new_benefit.display(i).benefit.getEps(), new_benefit.display(i).benefit.getPension_severance(), new_benefit.display(i).benefit.getCompensation_fund()});
          }   
        }  
      }
      else{
          JOptionPane.showMessageDialog(null, "Enter the data for delete the Benefit Service.");
      }
    }
    
    public void update(){
      if(!new_benefit.is_empty()){
        if(textArl.getText().length() > 0){
           Benefit_service benefit = new Benefit_service(textArl.getText(), textEps.getText(), textPension.getText(), textCompenssation.getText());
           new_benefit.update(textArl.getText(), benefit);
            clean_table();
            for(int i = 0; i < new_benefit.size(); i++){
              model_list.addRow(new Object[]{new_benefit.display(i).benefit.getArl(), new_benefit.display(i).benefit.getEps(), new_benefit.display(i).benefit.getPension_severance(), new_benefit.display(i).benefit.getCompensation_fund()});
            } 
           clear();
           JOptionPane.showMessageDialog(null, "Updated successfully.");
        }
        else{
          JOptionPane.showMessageDialog(null, "Search the vacant to update.");
        }   
      }
      else{
          JOptionPane.showMessageDialog(null, "The list is empty.");
      }
    }

    public void table() {

        ConexionSQL conexion = new ConexionSQL();

        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {

                Object id = jTableBenefit.getValueAt(row, 0);
                Object first = jTableBenefit.getValueAt(row, 1);
                Object second = jTableBenefit.getValueAt(row, 2);
                Object third = jTableBenefit.getValueAt(row, 3);
                Object fourth = jTableBenefit.getValueAt(row, 4);

                String[] fields = new String[4];

                fields[0] = first.toString();
                fields[1] = second.toString();
                fields[2] = third.toString();
                fields[3] = fourth.toString();

                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.updateRow(tableName, id.toString(), fields);
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
                return true;
            }
        };

        model.addColumn("id");
        model.addColumn("Arl");
        model.addColumn("Eps");
        model.addColumn("Pension");
        model.addColumn("Compensation Fund");

        this.jTableBenefit.setModel(model);
        jTableBenefit.getColumnModel().getColumn(0).setMinWidth(0);
        jTableBenefit.getColumnModel().getColumn(0).setMaxWidth(0);
        jTableBenefit.getColumnModel().getColumn(1).setPreferredWidth(105);
        jTableBenefit.getColumnModel().getColumn(2).setPreferredWidth(105);
        jTableBenefit.getColumnModel().getColumn(3).setPreferredWidth(105);
        jTableBenefit.getColumnModel().getColumn(4).setPreferredWidth(120);

        int employee_id = Integer.parseInt(jTextFieldEmployeeId.getText());
        String[][] result = conexion.tableRelation(tableName, employee_id);

        for (int i = 0; i < result.length; i++) {
            model.addRow(result[i]);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        btn_back_benefit = new javax.swing.JButton();
        jLabelTittle = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textArl = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textEps = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        textPension = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        textCompenssation = new javax.swing.JTextField();
        btnCreate = new javax.swing.JButton();
        jButtonUpdate = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextFieldId = new javax.swing.JTextField();
        jTextFieldEmployeeId = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableBenefit = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_benefit = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(549, 350));
        setSize(new java.awt.Dimension(549, 350));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N

        btn_back_benefit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back_benefit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_back_benefitActionPerformed(evt);
            }
        });

        jLabelTittle.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabelTittle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTittle.setText("Benefit service");

        jLabel2.setText(" ARL: ");

        textArl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textArlActionPerformed(evt);
            }
        });

        jLabel3.setText(" EPS:");

        jLabel4.setText(" Pension severance: ");

        jLabel5.setText(" compensation fund: ");

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        jButtonUpdate.setText("Update");
        jButtonUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateActionPerformed(evt);
            }
        });

        jButton1.setText("Confirm");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Delete");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTableBenefit.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableBenefit);

        jScrollPane2.setViewportView(table_benefit);

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addComponent(btn_back_benefit, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelTittle, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                                .addGap(6, 6, 6)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textEps, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textArl, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(52, 52, 52)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldEmployeeId, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textCompenssation, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textPension, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addContainerGap(44, Short.MAX_VALUE)
                        .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(54, 54, 54))
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_back_benefit, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTittle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textArl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(textEps, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldEmployeeId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textPension, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textCompenssation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreate)
                    .addComponent(jButtonUpdate)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void textArlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textArlActionPerformed

    }//GEN-LAST:event_textArlActionPerformed

    private void btn_back_benefitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_back_benefitActionPerformed
        Employees obj = new Employees();
        obj.setVisible(true);
        dispose();
    }//GEN-LAST:event_btn_back_benefitActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed

        String arl = textArl.getText();
        String eps = textEps.getText();
        String pensionSeverance = textPension.getText();
        String compensationFund = textCompenssation.getText();
        String employeeId = jTextFieldEmployeeId.getText();

        if (arl.isEmpty() || eps.isEmpty() || pensionSeverance.isEmpty() || compensationFund.isEmpty()) {
            JOptionPane.showMessageDialog(null, "The data are required.");
        } else {
            String[] fields = new String[5];

            fields[0] = arl;
            fields[1] = eps;
            fields[2] = pensionSeverance;
            fields[3] = compensationFund;
            fields[4] = employeeId;

            ConexionSQL conexion = new ConexionSQL();
            conexion.newRegister(tableName, fields);
            table();
        }
        add_elements();
    }//GEN-LAST:event_btnCreateActionPerformed

    private void jButtonUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateActionPerformed
        update();

        int selectedRow = jTableBenefit.getSelectedRow();

        if (selectedRow >= 0) {
            Object id = jTableBenefit.getValueAt(selectedRow, 0);
            Object arl = jTableBenefit.getValueAt(selectedRow, 1);
            Object eps = jTableBenefit.getValueAt(selectedRow, 2);
            Object pensionSeverance = jTableBenefit.getValueAt(selectedRow, 3);
            Object compensationFund = jTableBenefit.getValueAt(selectedRow, 4);

            try {
                jTextFieldId.setText(id.toString());
                textArl.setText(arl.toString());
                textEps.setText(eps.toString());
                textPension.setText(pensionSeverance.toString());
                textCompenssation.setText(compensationFund.toString());
                
            } catch (Exception e) {
                System.out.println("unsuccessfully" + e);
            }

        }
    }//GEN-LAST:event_jButtonUpdateActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String[] fields = new String[5];

        String id = jTextFieldId.getText();
        String arl = textArl.getText();
        String eps = textEps.getText();
        String pension = textPension.getText();
        String compesationFund = textCompenssation.getText();
        
        String empoyee_id = jTextFieldEmployeeId.getText();

        fields[0] = arl;
        fields[1] = eps;
        fields[2] = pension;
        fields[3] = compesationFund;
        fields[4] = empoyee_id;
        try {
            ConexionSQL conexion = new ConexionSQL();
            conexion.updateRow(tableName, id, fields);
        } catch (Exception e) {
            System.out.println("connection error:" + e);
        }
        table();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        delete();

        int selectedRow = jTableBenefit.getSelectedRow();
        if (selectedRow >= 0) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure ?");
            if (dialogResult == JOptionPane.YES_OPTION) {
                Object id = jTableBenefit.getValueAt(selectedRow, 0);
                model.removeRow(selectedRow);
                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.removeRow(tableName, id.toString());
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
            }
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Benefit_services.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Benefit_services.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Benefit_services.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Benefit_services.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Benefit_services().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btn_back_benefit;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButtonUpdate;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public javax.swing.JLabel jLabelTittle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableBenefit;
    public javax.swing.JTextField jTextFieldEmployeeId;
    private javax.swing.JTextField jTextFieldId;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JTable table_benefit;
    private javax.swing.JTextField textArl;
    private javax.swing.JTextField textCompenssation;
    private javax.swing.JTextField textEps;
    private javax.swing.JTextField textPension;
    // End of variables declaration//GEN-END:variables
}
