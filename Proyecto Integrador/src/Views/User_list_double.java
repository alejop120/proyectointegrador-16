package Views;
import Classes.ConexionSQL;
import Classes.User;
import Lists.Double_list;
import java.awt.Component;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author libardo sánchez
 */
public class User_list_double extends javax.swing.JFrame{
    Vector<Object>List_role;
    private Double_list list;
    DefaultTableModel model;
    String tableName = "user";
    
    public void setLocationRelativeTo(Component c){
      super.setLocationRelativeTo(c);
    }

    public User_list_double(){
      this.setTitle("Users");
      List_role =new Vector<Object>();
      initComponents();
      table();
      inputId.setVisible(false);
      list = new Double_list();
      selectionRole();
      setLocationRelativeTo(this);
    }
    
    public void table(){

        ConexionSQL conexion = new ConexionSQL();

        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {

                Object id = userTable.getValueAt(row, 0);
                Object first = userTable.getValueAt(row, 1);
                Object second = userTable.getValueAt(row, 2);
                Object third = userTable.getValueAt(row, 3);
                Object fourth = userTable.getValueAt(row, 4);
                Object fifth = userTable.getValueAt(row, 5);

                String[] fields = new String[5];

                fields[0] = first.toString();
                fields[1] = second.toString();
                fields[2] = third.toString();
                fields[3] = fourth.toString();
                fields[4] = fifth.toString();

                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.updateRow("user", id.toString(), fields);
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
                return true;
            }
        };
        model.addColumn("id");
        model.addColumn("Name");
        model.addColumn("Surname");
        model.addColumn("Email");
        model.addColumn("Document");
        model.addColumn("Role");

        this.userTable.setModel(model);
        userTable.getColumnModel().getColumn(0).setMinWidth(0);
        userTable.getColumnModel().getColumn(0).setMaxWidth(0);
        userTable.getColumnModel().getColumn(1).setPreferredWidth(105);
        userTable.getColumnModel().getColumn(2).setPreferredWidth(105);
        userTable.getColumnModel().getColumn(3).setPreferredWidth(105);
        userTable.getColumnModel().getColumn(4).setPreferredWidth(120);
        userTable.getColumnModel().getColumn(5).setPreferredWidth(90);

        String[][] result = conexion.allTable(tableName);

        for(int i = 0; i < result.length; i++){
          model.addRow(result[i]);
        }
    }
    
    public void selectionRole(){
      List_role.add("ALL PRIVILEGES");
      List_role.add("CREATE");
      List_role.add("DROP");
      List_role.add("DELETE");
      List_role.add("INSERT");
      List_role.add("SELECT");
      List_role.add("UPDATE");

      for(int i = 0; i < List_role.size(); i++){
        selectRole.addItem(List_role.get(i).toString());
      }
    }
    
    public void clear(){
      textName.setText(null);
      textSurname.setText(null);
      textEmail.setText(null);
      textDocument.setText(null);
      birthdate_double.setDateFormatString(null);
    }

    public void set_client_position(){
      int value = Integer.parseInt(JOptionPane.showInputDialog("Enter the previous position:"));
      User client = new User(textName.getText(),textSurname.getText(), textDocument.getText(), selectRole.getSelectedItem().toString());
      list.insert_position(client, value);
      JOptionPane.showMessageDialog(null,"You added a new user");
      clear();       
     }
    
    public void set_client_last(){
      User client = new User(textName.getText(),textSurname.getText(), textDocument.getText(), selectRole.getSelectedItem().toString());
      list.insert_last(client);
      JOptionPane.showMessageDialog(null,"You added a new user");
      clear();
    }

    public void set_client_initial(){
      User client = new User(textName.getText(),textSurname.getText(), textDocument.getText(), selectRole.getSelectedItem().toString());
      list.insert_initial(client);
      JOptionPane.showMessageDialog(null,"A user is added to the beginning");
      clear();
     }
    
    public void searches_document(){
      String cedula =JOptionPane.showInputDialog("Enter the document search") ;
      if(list.search(cedula) != null){
        JOptionPane.showMessageDialog(null,list.search(cedula).data);
        jTextArea1.setText(list.search(cedula).data.toString());
      }
      else{
        JOptionPane.showMessageDialog(null,"The user not exist!!!" );
      }
    }
    
    public void show_datas(){
      jTextArea1.setText(list.read());
    }
    
    public void delete_user(){
      int valor =Integer.parseInt(JOptionPane.showInputDialog("Enter the position to delete")) ;
      list.delete(valor);
      JOptionPane.showMessageDialog(null, "You deleted a user");   
    }
    
    public void delete_client(){
      list.delete_last();
      JOptionPane.showMessageDialog(null, "You deleted a user");
    }
    
    public void update_user(){
      String valor =JOptionPane.showInputDialog("Enter the document of user");
      User client = new User(textName.getText(),textSurname.getText(), textDocument.getText(), selectRole.getSelectedItem().toString());
      list.update(valor, client);
      JOptionPane.showMessageDialog(rootPane,"User updated");
      clear();
    }
    
    public void select_operation_user(){
      if(select_operation_client.getSelectedItem().equals("Delete by position")){
          delete_user();
      } 
        
      if(select_operation_client.getSelectedItem().equals("Delete the start")){
         list.delete_initial();
         JOptionPane.showMessageDialog(null, "User deleted");
      }
  
      if(select_operation_client.getSelectedItem().equals("Delete last")){
         delete_client();
      }

      if(select_operation_client.getSelectedItem().equals("Insert by position")){
         set_client_position();
      }
        
      if(select_operation_client.getSelectedItem().equals("Insert the start")){
         set_client_initial(); 
      }
        
      if(select_operation_client.getSelectedItem().equals("Count users")){
        JOptionPane.showMessageDialog(null, "Total users: " + (list.size()));
      }
        
      if(select_operation_client.getSelectedItem().equals("Update")){
         update_user();
      }
        
      if(select_operation_client.getSelectedItem().equals("Search by document")){
        searches_document();
      } 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textSurname = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        textDocument = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        selectRole = new javax.swing.JComboBox();
        select_operation_client = new javax.swing.JComboBox();
        calcular = new javax.swing.JToggleButton();
        agregar = new javax.swing.JButton();
        buscar = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        mostrar = new javax.swing.JButton();
        btn_clear = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        birthdate_double = new com.toedter.calendar.JDateChooser();
        jScrollPane2 = new javax.swing.JScrollPane();
        userTable = new javax.swing.JTable();
        update = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        textEmail = new javax.swing.JTextField();
        inputId = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(650, 490));
        setSize(new java.awt.Dimension(650, 490));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background_views.jpg"))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Malayalam Sangam MN", 0, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Users");

        jLabel3.setText("Name: ");

        jLabel2.setText(" Surname:");

        textSurname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textSurnameActionPerformed(evt);
            }
        });

        jLabel1.setText(" Document:");

        textDocument.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textDocumentActionPerformed(evt);
            }
        });

        jLabel5.setText("Birt date: ");

        jLabel4.setText(" Privileges:");

        selectRole.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectRoleActionPerformed(evt);
            }
        });

        select_operation_client.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Insert by position", "Insert the start", "Delete by position", "Delete the start", "Delete last", "Count users", "Update", "Search by document", " " }));
        select_operation_client.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                select_operation_clientActionPerformed(evt);
            }
        });

        calcular.setText("Accept");
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        agregar.setText("Create");
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });

        buscar.setText("Search");
        buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarActionPerformed(evt);
            }
        });

        jButton3.setText("Delete");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        mostrar.setText("Show");
        mostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarActionPerformed(evt);
            }
        });

        btn_clear.setText("Clear");
        btn_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_clearActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel7.setText(" Operation:");

        userTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(userTable);

        update.setText("Update");
        update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateActionPerformed(evt);
            }
        });

        jLabel8.setText("Email:");

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jButton3)
                                    .addComponent(agregar))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addComponent(mostrar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buscar))
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addComponent(btn_clear, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(update)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(inputId, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(24, 24, 24))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(5, 5, 5)
                                        .addComponent(textSurname))
                                    .addGroup(panelImage1Layout.createSequentialGroup()
                                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(panelImage1Layout.createSequentialGroup()
                                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                            .addGroup(panelImage1Layout.createSequentialGroup()
                                                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addGap(6, 6, 6)))
                                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(textName, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                                            .addComponent(textDocument))))
                                .addGap(52, 52, 52)))
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE))
                                .addGap(6, 6, 6)
                                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(selectRole, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(birthdate_double, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelImage1Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(7, 7, 7)
                                .addComponent(select_operation_client, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(calcular, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(38, Short.MAX_VALUE))
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textSurname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textDocument, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(agregar)
                            .addComponent(mostrar)
                            .addComponent(buscar))
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(textEmail, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(birthdate_double, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                                .addComponent(selectRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(select_operation_client, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)))
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton3)
                        .addComponent(btn_clear)
                        .addComponent(update)
                        .addComponent(inputId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(435, 435, 435)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelImage1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateActionPerformed
        textName.setText(null);
        textSurname.setText(null);
        textEmail.setText(null);
        textDocument.setText(null);

        int selectedRow = userTable.getSelectedRow();

        if(selectedRow >= 0){
            int col = 0;
            Object id = userTable.getValueAt(selectedRow, col);
            col = 1;
            Object name = userTable.getValueAt(selectedRow, col);
            col = 2;
            Object surname = userTable.getValueAt(selectedRow, col);
            col = 3;
            Object email = userTable.getValueAt(selectedRow, col);
            col = 4;
            Object document = userTable.getValueAt(selectedRow, col);
            col = 5;
            Object role = "1";

            inputId.setText(id.toString());
            textName.setText(name.toString());
            textName.requestFocus();
            textSurname.setText(surname.toString());
            textEmail.setText(email.toString());
            textDocument.setText(document.toString());
            //selectRole
        }
    }//GEN-LAST:event_updateActionPerformed


    private void btn_clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_clearActionPerformed
        jTextArea1.setText(null);
        clear();
    }//GEN-LAST:event_btn_clearActionPerformed

    private void mostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrarActionPerformed
        show_datas();
    }//GEN-LAST:event_mostrarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int selectedRow = userTable.getSelectedRow();
        if (selectedRow >= 0) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure ?");
            if (dialogResult == JOptionPane.YES_OPTION) {
                Object id = userTable.getValueAt(selectedRow, 0);
                model.removeRow(selectedRow);
                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.removeRow(tableName, id.toString());
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
            }
        }
        delete_client();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarActionPerformed
        searches_document();
    }//GEN-LAST:event_buscarActionPerformed

    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        String name = textName.getText();
        String surname = textSurname.getText();
        String email = textEmail.getText();
        String document = textDocument.getText();
        String role = "1";

        String[] fields = new String[5];

        fields[0] = name;
        fields[1] = surname;
        fields[2] = email;
        fields[3] = document;
        fields[4] = role;

        ConexionSQL conexion = new ConexionSQL();
        conexion.newRegister(tableName, fields);
        table();
        set_client_last();
    }//GEN-LAST:event_agregarActionPerformed

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
        
        String[] fields = new String[5];

        String id = inputId.getText();
        String name = textName.getText();
        String surname = textSurname.getText();
        String email = textEmail.getText();
        String document = textDocument.getText();
        String role = "1";

        fields[0] = name;
        fields[1] = surname;
        fields[2] = email;
        fields[3] = document;
        fields[4] = role;

        try {
            ConexionSQL conexion = new ConexionSQL();
            conexion.updateRow(tableName, id, fields);
        } catch (Exception e) {
            System.out.println("connection error:" + e);
        }
        table();
        select_operation_user();
    }//GEN-LAST:event_calcularActionPerformed

    private void select_operation_clientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_select_operation_clientActionPerformed

    }//GEN-LAST:event_select_operation_clientActionPerformed

    private void textDocumentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textDocumentActionPerformed

    }//GEN-LAST:event_textDocumentActionPerformed

    private void textSurnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textSurnameActionPerformed

    }//GEN-LAST:event_textSurnameActionPerformed

    private void selectRoleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectRoleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_selectRoleActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      dispose();
      new Selection_program().setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(User_list_double.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(User_list_double.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(User_list_double.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(User_list_double.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new User_list_double().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agregar;
    private com.toedter.calendar.JDateChooser birthdate_double;
    private javax.swing.JButton btn_clear;
    private javax.swing.JButton buscar;
    private javax.swing.JToggleButton calcular;
    private javax.swing.JTextField inputId;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton mostrar;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JComboBox selectRole;
    private javax.swing.JComboBox select_operation_client;
    private javax.swing.JTextField textDocument;
    private javax.swing.JTextField textEmail;
    private javax.swing.JTextField textName;
    private javax.swing.JTextField textSurname;
    private javax.swing.JButton update;
    private javax.swing.JTable userTable;
    // End of variables declaration//GEN-END:variables
}
