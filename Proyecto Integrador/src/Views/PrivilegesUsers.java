package Views;
import Classes.ConexionSQL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author josealejandro
 */
public class PrivilegesUsers extends javax.swing.JFrame {
    String tableName = "user";
    DefaultTableModel model;
    ConexionSQL conexionSql = new ConexionSQL();

    public PrivilegesUsers() {
        initComponents();
        this.setTitle("Privileges Users");
        this.setLocationRelativeTo(this);
        table();
    }
    
    public void table() {

        model = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnIndex == 0 ? super.getColumnClass(columnIndex) : Boolean.class;
            }

            @Override
            public void setValueAt(Object aValue, int row, int column) {
                super.setValueAt(aValue, row, column); //To change body of generated methods, choose Tools | Templates.
                Object name = jTablePrivileges.getValueAt(row, 0);
                String consult = "";
                if(column == 1){
                    if (aValue.toString().equalsIgnoreCase("true")) {
                        consult = "GRANT CREATE ON *.* TO '" + name + "'@'localhost'";
                    }else{
                        consult = "REVOKE CREATE ON *.* FROM '" + name + "'@'localhost'";
                    }
                }
                if (column == 2) {
                    if (aValue.toString().equalsIgnoreCase("true")) {
                        consult = "GRANT DROP ON *.* TO '" + name + "'@'localhost'";
                    } else {
                        consult = "REVOKE DROP ON *.* FROM '" + name + "'@'localhost'";
                    }
                }
                if (column == 3) {
                    if (aValue.toString().equalsIgnoreCase("true")) {
                        consult = "GRANT DELETE ON *.* TO '" + name + "'@'localhost'";
                    } else {
                        consult = "REVOKE DELETE ON *.* FROM '" + name + "'@'localhost'";
                    }
                }
                if (column == 4) {
                    if (aValue.toString().equalsIgnoreCase("true")) {
                        consult = "GRANT INSERT ON *.* TO '" + name + "'@'localhost'";
                    } else {
                        consult = "REVOKE INSERT ON *.* FROM '" + name + "'@'localhost'";
                    }
                }
                if (column == 5) {
                    if (aValue.toString().equalsIgnoreCase("true")) {
                        consult = "GRANT SELECT ON *.* TO '" + name + "'@'localhost'";
                    } else {
                        consult = "REVOKE SELECT ON *.* FROM '" + name + "'@'localhost'";
                    }
                }
                if (column == 6) {
                    if (aValue.toString().equalsIgnoreCase("true")) {
                        consult = "GRANT UPDATE ON *.* TO '" + name + "'@'localhost'";
                    } else {
                        consult = "REVOKE UPDATE ON *.* FROM '" + name + "'@'localhost'";
                    }
                }
                try {
                    PreparedStatement pst = conexionSql.conexion.prepareStatement(consult);
                    int n =  pst.executeUpdate();
                    
                    if(n>0){
                      System.out.println("Exito");
                    }
                    PreparedStatement updatePrivileges = conexionSql.conexion.prepareStatement("FLUSH PRIVILEGES");
                    updatePrivileges.executeUpdate();
                    
                } catch (Exception e) {
                    System.out.println("unsuccessfully: "+e);
                }
            }
        };
        
        model.addColumn("User");
        model.addColumn("CREATE");
        model.addColumn("DROP");
        model.addColumn("DELETE");
        model.addColumn("INSERT");
        model.addColumn("SELECT");
        model.addColumn("UPDATE");

        jTablePrivileges.setModel(model);
        
        try {
            String consult = "select * from user";
            Statement statement = conexionSql.conexionToMysql.createStatement();
            ResultSet result = statement.executeQuery(consult);

            while (result.next()) {
                
                Object[] object = new Object[7];
                
                String user = result.getString(2);
                object[0] = user;
                boolean create = result.getBoolean("Create_priv");
                object[1] = create;
                boolean drop = result.getBoolean("Drop_priv");
                object[2] = drop;
                boolean delete = result.getBoolean("Delete_priv");
                object[3] = delete;
                boolean insert = result.getBoolean("Insert_priv");
                object[4] = insert;
                boolean select = result.getBoolean("Select_priv");
                object[5] = select;
                boolean update = result.getBoolean("Update_priv");
                object[6] = update;
                
                model.addRow(object);
            } 
        }catch (Exception e) {
              System.out.println("unsuccessfully: " + e);
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePrivileges = new javax.swing.JTable();
        jButtonAddUser = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jCheckBoxCreate = new javax.swing.JCheckBox();
        jCheckBoxDrop = new javax.swing.JCheckBox();
        jTextFieldPassword = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jCheckBoxDelete = new javax.swing.JCheckBox();
        jCheckBoxInsert = new javax.swing.JCheckBox();
        jCheckBoxSelect = new javax.swing.JCheckBox();
        jCheckBoxUpdate = new javax.swing.JCheckBox();
        btn_back = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Privileges users");
        jLabel1.setToolTipText("");
        jLabel1.setSize(new java.awt.Dimension(45, 25));

        jTablePrivileges.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTablePrivileges);

        jButtonAddUser.setText("Add a new user");
        jButtonAddUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddUserActionPerformed(evt);
            }
        });

        jLabel2.setText("Name");

        jCheckBoxCreate.setText("CREATE");

        jCheckBoxDrop.setText("DROP");

        jLabel3.setText("Password");

        jCheckBoxDelete.setText("DELETE");

        jCheckBoxInsert.setText("INSERT");

        jCheckBoxSelect.setText("SELECT");

        jCheckBoxUpdate.setText("UPDATE");

        btn_back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        btn_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAddUser)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jCheckBoxCreate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBoxDrop)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxDelete)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxInsert)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxSelect)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBoxUpdate))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 628, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(btn_back, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_back))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxCreate)
                    .addComponent(jCheckBoxDrop)
                    .addComponent(jCheckBoxDelete)
                    .addComponent(jCheckBoxInsert)
                    .addComponent(jCheckBoxSelect)
                    .addComponent(jCheckBoxUpdate))
                .addGap(18, 18, 18)
                .addComponent(jButtonAddUser)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddUserActionPerformed
        // TODO add your handling code here:
        String name = jTextFieldName.getText();
        String password = jTextFieldPassword.getText();
        
        if(name.isEmpty() || password.isEmpty()){
            JOptionPane.showMessageDialog(null, "Campos incompletos");
        }else{
            try {
                String newUser = "CREATE USER '" + name + "'@'localhost' IDENTIFIED BY '" + password + "'";
                PreparedStatement pst; 
                
                pst = conexionSql.conexion.prepareStatement(newUser);
                pst.executeUpdate();
                
                boolean create = false;
                boolean drop = false;
                boolean delete = false;
                boolean insert = false;
                boolean select = false;
                boolean update = false;
                
                if(jCheckBoxCreate.isSelected()){
                    create = true;
                    String consult = "GRANT CREATE ON human_resources.* TO '" + name + "'@'localhost'";
                    pst = conexionSql.conexion.prepareStatement(consult);
                    pst.executeUpdate();
                }
                if (jCheckBoxDrop.isSelected()) {
                    drop = true;
                    String consult = "GRANT DROP ON human_resources.* TO '" + name + "'@'localhost'";
                    pst = conexionSql.conexion.prepareStatement(consult);
                    pst.executeUpdate();
                }
                if (jCheckBoxDelete.isSelected()) {
                    insert = true;
                    String consult = "GRANT DELETE ON human_resources.* TO '" + name + "'@'localhost'";
                    pst = conexionSql.conexion.prepareStatement(consult);
                    pst.executeUpdate();
                }
                if (jCheckBoxInsert.isSelected()) {
                    delete = true;
                    String consult = "GRANT INSERT ON human_resources.* TO '" + name + "'@'localhost'";
                    pst = conexionSql.conexion.prepareStatement(consult);
                    pst.executeUpdate();
                }
                if (jCheckBoxSelect.isSelected()) {
                    select = true;
                    String consult = "GRANT SELECT ON human_resources.* TO '" + name + "'@'localhost'";
                    pst = conexionSql.conexion.prepareStatement(consult);
                    pst.executeUpdate();
                }
                if (jCheckBoxUpdate.isSelected()) {
                    update = true;
                    String consult = "GRANT UPDATE ON human_resources.* TO '" + name + "'@'localhost'";
                    pst = conexionSql.conexion.prepareStatement(consult);
                    pst.executeUpdate();
                }
                
                Object[] object = new Object[7];

                object[0] = name;
                object[1] = create;
                object[2] = drop;
                object[3] = delete;
                object[4] = insert;
                object[5] = select;
                object[6] = update;
                model.addRow(object);
                
            } catch (Exception e) {
                System.out.println("unsuccessfully: " + e);
            }
        }
    }//GEN-LAST:event_jButtonAddUserActionPerformed

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backActionPerformed
       dispose();
       new Selection_program().setVisible(true);
    }//GEN-LAST:event_btn_backActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PrivilegesUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PrivilegesUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PrivilegesUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PrivilegesUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrivilegesUsers().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_back;
    private javax.swing.JButton jButtonAddUser;
    private javax.swing.JCheckBox jCheckBoxCreate;
    private javax.swing.JCheckBox jCheckBoxDelete;
    private javax.swing.JCheckBox jCheckBoxDrop;
    private javax.swing.JCheckBox jCheckBoxInsert;
    private javax.swing.JCheckBox jCheckBoxSelect;
    private javax.swing.JCheckBox jCheckBoxUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTablePrivileges;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldPassword;
    // End of variables declaration//GEN-END:variables
}
