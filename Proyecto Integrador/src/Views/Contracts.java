package Views;
import Classes.ComboItem;
import Classes.ConexionSQL;
import Classes.Contract;
import Lists.Simple_list_contract;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 * @author josealejandro
 */
public class Contracts extends javax.swing.JFrame {
    DefaultTableModel model;
    DefaultTableModel model_list;
    String tableName = "contract";
    ConexionSQL conexionSql;

    public Contracts() {
        initComponents();
        jTextFieldEmployeeId.setVisible(false);
        jTextFieldId.setVisible(false);
        this.setTitle("Contracts");
        this.setLocationRelativeTo(this);
        try {
            conexionSql = new ConexionSQL();
            String consult = "select * from typecontract";
            Statement statement = conexionSql.conexion.createStatement();
            ResultSet result = statement.executeQuery(consult);
            
            DefaultComboBoxModel modelComboBox = new DefaultComboBoxModel();
            modelComboBox.addElement(new ComboItem("Seleccione un tipo", null));
            while(result.next()){
                int id = result.getInt("typeContract_id");
                String name = result.getString("name");
                modelComboBox.addElement(new ComboItem(name, String.valueOf(id)));
                comboBoxType.setModel(modelComboBox);
            }
        } catch (Exception e) {
            System.out.println("unsuccessfully:"+ e);
        }
        
      model_list = new DefaultTableModel(){
      @Override
        public boolean isCellEditable(int row, int column){
          Object first = table_contract.getValueAt(row, 0);
          Object second = table_contract.getValueAt(row, 1);
          Object third = table_contract.getValueAt(row, 2);

          String[] fields = new String[3];

          fields[0] = first.toString();
          fields[1] = second.toString();
          fields[2] = third.toString();

          return true;
        }
      };
    
      model_list.addColumn("Position");
      model_list.addColumn("Date");
      model_list.addColumn("Salary");

      this.table_contract.setModel(model_list);
      table_contract.getColumnModel().getColumn(0).setPreferredWidth(132);
      table_contract.getColumnModel().getColumn(1).setPreferredWidth(132);
      table_contract.getColumnModel().getColumn(2).setPreferredWidth(132);
    }
    
    Simple_list_contract new_contract = new Simple_list_contract();
    
    public void clean(){
      text_position.setText(null);
      text_date.setDate(null);
      text_salary.setText(null);
      DefaultComboBoxModel modelComboBox = new DefaultComboBoxModel();
      modelComboBox.addElement(new ComboItem("Seleccione un tipo", null));
    }
    
    public void clean_table(){
      try{
         int rows = table_contract.getRowCount();
          if (rows > 0){
            for(int i = 0; rows > i; i++){
               model_list.removeRow(0);
            }
          }
      }
      catch(Exception e){
        JOptionPane.showMessageDialog(null, "Failed to clean the table.");
      }
    }
    
    public void add_elements(){
      if(text_position.getText().length() > 0 && text_salary.getText().length() > 0){
        Date dateFromDateChooser = text_date.getDate();
        String date = String.format("%1$td-%1$tm-%1$tY", dateFromDateChooser);
        Contract contract = new Contract(text_position.getText(), date, Integer.parseInt(text_salary.getText()) );
        new_contract.insert(contract);
        if(!new_contract.is_empty()){
          clean_table();
          for(int i = 0; i < new_contract.size(); i++){
            model_list.addRow(new Object[]{new_contract.display(i).contract.getPosition(), new_contract.display(i).contract.getDate(), new_contract.display(i).contract.getSalary()});
          } 
        }
        clean();
        JOptionPane.showMessageDialog(null, "Create successfully");  
      }
      else{
        JOptionPane.showMessageDialog(null, "The data are required for create.");
      }
    }
    
    public void delete(){
      Date dateFromDateChooser = text_date.getDate();
      String date = String.format("%1$td-%1$tm-%1$tY", dateFromDateChooser);
      if(text_position.getText().length() > 0 && date.length() > 0 && text_salary.getText().length() > 0 ){
        new_contract.delete_list(text_position.getText(), date, Integer.parseInt(text_salary.getText()));
        clean();
        clean_table();
         if(!new_contract.is_empty()){
          for(int i = 0; i < new_contract.size(); i++){
            model_list.addRow(new Object[]{new_contract.display(i).contract.getPosition(), new_contract.display(i).contract.getDate(), new_contract.display(i).contract.getSalary()});
          }   
         }
      }
      else{
          JOptionPane.showMessageDialog(null, "Enter the data by delete the contract.");
      }
    }
    
    public void update(){
      if(!new_contract.is_empty()){
        if(text_position.getText().length() > 0){
           Date dateFromDateChooser = text_date.getDate();
           String date = String.format("%1$td-%1$tm-%1$tY", dateFromDateChooser);
           Contract contract = new Contract(text_position.getText(), date, Integer.parseInt(text_salary.getText()));
           new_contract.update(text_position.getText(), contract);
            clean_table();
            for(int i = 0; i < new_contract.size(); i++){
              
              model_list.addRow(new Object[]{new_contract.display(i).contract.getPosition(), new_contract.display(i).contract.getDate(), new_contract.display(i).contract.getSalary()});
            } 
           clean();
           JOptionPane.showMessageDialog(null, "Updated successfully.");
        }
        else{
          JOptionPane.showMessageDialog(null, "Search the contract to update.");
        }   
      }
      else{
          JOptionPane.showMessageDialog(null, "The list is empty.");
      }
    }
    
    public void table() {

        ConexionSQL conexion = new ConexionSQL();

        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {

                Object id = jTableContracts.getValueAt(row, 0);
                Object first = jTableContracts.getValueAt(row, 1);
                Object second = jTableContracts.getValueAt(row, 2);
                Object third = jTableContracts.getValueAt(row, 3);
                Object fourth = jTableContracts.getValueAt(row, 4);
                Object fifth = jTextFieldEmployeeId.getText();

                String[] fields = new String[5];

                fields[0] = first.toString();
                fields[1] = second.toString();
                fields[2] = third.toString();
                fields[3] = fourth.toString();
                fields[4] = fifth.toString();

                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.updateRow(tableName, id.toString(), fields);
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
                return true;
            }
        };

        model.addColumn("id");
        model.addColumn("Date");
        model.addColumn("Salary");
        model.addColumn("Position");
        model.addColumn("Type");

        this.jTableContracts.setModel(model);
        jTableContracts.getColumnModel().getColumn(0).setMinWidth(0);
        jTableContracts.getColumnModel().getColumn(0).setMaxWidth(0);
        jTableContracts.getColumnModel().getColumn(1).setPreferredWidth(105);
        jTableContracts.getColumnModel().getColumn(2).setPreferredWidth(105);
        jTableContracts.getColumnModel().getColumn(3).setPreferredWidth(105);
        jTableContracts.getColumnModel().getColumn(4).setPreferredWidth(120);
        
        int employee_id = Integer.parseInt(jTextFieldEmployeeId.getText());
        String[][] result = conexion.tableRelation(tableName, employee_id);

        for(int i = 0; i < result.length; i++) {
            model.addRow(result[i]);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        text_date = new com.toedter.calendar.JDateChooser();
        text_position = new javax.swing.JTextField();
        text_salary = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableContracts = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        comboBoxType = new javax.swing.JComboBox<>();
        jTextFieldEmployeeId = new javax.swing.JTextField();
        jButtonCreate = new javax.swing.JButton();
        jButtonUpdate = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();
        jButtonEndowments = new javax.swing.JButton();
        jButtonConfirm = new javax.swing.JButton();
        jTextFieldId = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_contract = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(654, 490));
        setSize(new java.awt.Dimension(654, 490));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Contracts");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/back.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Position:");

        jLabel3.setText("Date:");

        jLabel4.setText("Salary");

        jTableContracts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableContracts);

        jLabel5.setText("Type:");

        comboBoxType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxTypeActionPerformed(evt);
            }
        });

        jButtonCreate.setText("Create");
        jButtonCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateActionPerformed(evt);
            }
        });

        jButtonUpdate.setText("Update");
        jButtonUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateActionPerformed(evt);
            }
        });

        jButtonDelete.setText("Delete");
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        jButtonEndowments.setText("Endowments");
        jButtonEndowments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEndowmentsActionPerformed(evt);
            }
        });

        jButtonConfirm.setText("Confirm");
        jButtonConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConfirmActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(table_contract);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 635, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 635, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonCreate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonUpdate))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(text_salary)
                                    .addComponent(text_position, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(comboBoxType, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(text_date, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jButtonDelete)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonEndowments)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldEmployeeId, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jButtonConfirm)))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 612, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(text_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldEmployeeId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(comboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(text_position, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(text_salary, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonEndowments)
                    .addComponent(jButtonDelete)
                    .addComponent(jButtonUpdate)
                    .addComponent(jButtonCreate)
                    .addComponent(jButtonConfirm))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Employees obj = new Employees();
        obj.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButtonCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateActionPerformed
        
        String date = String.valueOf(text_date.getDate());
        String salary = text_salary.getText();
        String position = text_position.getText();
        ComboItem item = (ComboItem) comboBoxType.getSelectedItem();
        String type_id = item.getId();
        String employeeId = jTextFieldEmployeeId.getText();
        
        if(date.length() <= 4 || salary.isEmpty() || position.isEmpty() || item.getId() == null || employeeId.isEmpty() ){
            JOptionPane.showMessageDialog(null, "Todos lo campos son requeridos");
        }else{
            String[] fields = new String[5];

            fields[0] = date;
            fields[1] = salary;
            fields[2] = position;
            fields[3] = type_id;
            fields[4] = employeeId;

            ConexionSQL conexion = new ConexionSQL();
            conexion.newRegister(tableName, fields);
            table();
        }
        add_elements();
    }//GEN-LAST:event_jButtonCreateActionPerformed

    private void comboBoxTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxTypeActionPerformed

    }//GEN-LAST:event_comboBoxTypeActionPerformed

    private void jButtonUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateActionPerformed
        
        int selectedRow = jTableContracts.getSelectedRow();

        if (selectedRow >= 0) {
            int col = 0;
            Object id = jTableContracts.getValueAt(selectedRow, col);
            Object date = jTableContracts.getValueAt(selectedRow, col+=1);
            Object salary = jTableContracts.getValueAt(selectedRow, col+=1);
            Object position = jTableContracts.getValueAt(selectedRow, col+=1);
            Object type_id = jTableContracts.getValueAt(selectedRow, col+=1);
            
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                Date dateOriginal = formatter.parse(date.toString());

                jTextFieldId.setText(id.toString());
                text_date.setDate(dateOriginal);
                comboBoxType.setSelectedIndex(Integer.parseInt(type_id.toString()));
                text_position.setText(position.toString());
                text_salary.setText(salary.toString());
            } catch (Exception e) {
                System.out.println("unsuccessfully" + e);
            }
                                
        }
        update();
//        clean();
    }//GEN-LAST:event_jButtonUpdateActionPerformed

    private void jButtonConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConfirmActionPerformed
        String[] fields = new String[6];

        String id = jTextFieldId.getText();
        String date = String.valueOf(text_date.getDate());
        String salary = text_salary.getText();
        String position = text_position.getText();
        ComboItem item = (ComboItem) comboBoxType.getSelectedItem();
        String type_id = item.getId();
        String empoyee_id = jTextFieldEmployeeId.getText();

        fields[0] = date;
        fields[1] = salary;
        fields[2] = position;
        fields[3] = type_id;
        fields[4] = empoyee_id;

        try {
            ConexionSQL conexion = new ConexionSQL();
            conexion.updateRow(tableName, id, fields);
        } catch (Exception e) {
            System.out.println("connection error:" + e);
        }
        table();
    }//GEN-LAST:event_jButtonConfirmActionPerformed

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        delete();
        
        int selectedRow = jTableContracts.getSelectedRow();
        if (selectedRow >= 0) {
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure ?");
            if (dialogResult == JOptionPane.YES_OPTION) {
                Object id = jTableContracts.getValueAt(selectedRow, 0);
                model.removeRow(selectedRow);
                try {
                    ConexionSQL conexion = new ConexionSQL();
                    conexion.removeRow(tableName, id.toString());
                } catch (Exception e) {
                    System.out.println("connection error:" + e);
                }
            }
        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonEndowmentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEndowmentsActionPerformed
        int selectedRow = jTableContracts.getSelectedRow();
        if (selectedRow >= 0) {
            Object id = jTableContracts.getValueAt(selectedRow, 0);
            Endowment obj = new Endowment();
            obj.jTextFieldContractId.setText(id.toString());
            obj.table();
            obj.setVisible(true);
            dispose();
        }
    }//GEN-LAST:event_jButtonEndowmentsActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Contracts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Contracts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Contracts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Contracts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Contracts().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboBoxType;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonConfirm;
    private javax.swing.JButton jButtonCreate;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonEndowments;
    private javax.swing.JButton jButtonUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableContracts;
    public javax.swing.JTextField jTextFieldEmployeeId;
    private javax.swing.JTextField jTextFieldId;
    private javax.swing.JTable table_contract;
    private com.toedter.calendar.JDateChooser text_date;
    private javax.swing.JTextField text_position;
    private javax.swing.JTextField text_salary;
    // End of variables declaration//GEN-END:variables
}
