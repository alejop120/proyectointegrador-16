package Views;
import Views.*;
import Home.Index;
/**
 * @author libardo sánchez
 */
public class Selection_program extends javax.swing.JFrame {
    public Selection_program(){
      initComponents();
      this.setTitle("Home");
      this.setLocationRelativeTo(this);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelPrograms = new javax.swing.JPanel();
        panelImage1 = new org.edisoncor.gui.panel.PanelImage();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        file_option = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        list_simple = new javax.swing.JMenuItem();
        list_double = new javax.swing.JMenuItem();
        privileges_user = new javax.swing.JMenuItem();
        new_employee = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        show_payments = new javax.swing.JMenuItem();
        show_reports = new javax.swing.JMenuItem();
        item_signOut = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        show_types = new javax.swing.JMenuItem();
        hire = new javax.swing.JMenu();
        show_vacants = new javax.swing.JMenuItem();
        show_cvs = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(650, 490));

        panelImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/desktop.jpg"))); // NOI18N
        panelImage1.setPreferredSize(new java.awt.Dimension(650, 477));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/desktop_one.png"))); // NOI18N

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/desktop_two.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Marion", 0, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 204, 204));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("WELCOME TO OUR COMPANY");

        javax.swing.GroupLayout panelImage1Layout = new javax.swing.GroupLayout(panelImage1);
        panelImage1.setLayout(panelImage1Layout);
        panelImage1Layout.setHorizontalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelImage1Layout.createSequentialGroup()
                        .addGap(0, 461, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(panelImage1Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 496, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelImage1Layout.setVerticalGroup(
            panelImage1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelImage1Layout.createSequentialGroup()
                .addContainerGap(52, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addGap(49, 49, 49)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout panelProgramsLayout = new javax.swing.GroupLayout(panelPrograms);
        panelPrograms.setLayout(panelProgramsLayout);
        panelProgramsLayout.setHorizontalGroup(
            panelProgramsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, 653, Short.MAX_VALUE)
        );
        panelProgramsLayout.setVerticalGroup(
            panelProgramsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelImage1, javax.swing.GroupLayout.DEFAULT_SIZE, 439, Short.MAX_VALUE)
        );

        jMenuBar1.setBackground(new java.awt.Color(204, 204, 204));
        jMenuBar1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(102, 102, 102), new java.awt.Color(102, 102, 102), new java.awt.Color(102, 102, 102), new java.awt.Color(102, 102, 102)));
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        file_option.setText("   Staff   ");

        jMenu2.setText(" Users");

        list_simple.setText(" User List Simple");
        list_simple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list_simpleActionPerformed(evt);
            }
        });
        jMenu2.add(list_simple);

        list_double.setText(" User List Double");
        list_double.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                list_doubleActionPerformed(evt);
            }
        });
        jMenu2.add(list_double);

        privileges_user.setText(" Privileges Of User");
        privileges_user.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                privileges_userActionPerformed(evt);
            }
        });
        jMenu2.add(privileges_user);

        file_option.add(jMenu2);

        new_employee.setText(" Employees");

        jMenuItem1.setText(" New employee");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        new_employee.add(jMenuItem1);

        show_payments.setText(" Payments");
        show_payments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_paymentsActionPerformed(evt);
            }
        });
        new_employee.add(show_payments);

        show_reports.setText(" Labor report");
        show_reports.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_reportsActionPerformed(evt);
            }
        });
        new_employee.add(show_reports);

        file_option.add(new_employee);

        item_signOut.setText(" Exit");
        item_signOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                item_signOutActionPerformed(evt);
            }
        });
        file_option.add(item_signOut);

        jMenuBar1.add(file_option);

        jMenu1.setText("  Contracts  ");

        show_types.setText(" Contract types ");
        show_types.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_typesActionPerformed(evt);
            }
        });
        jMenu1.add(show_types);

        jMenuBar1.add(jMenu1);

        hire.setText("    Hire   ");

        show_vacants.setText(" Vacants");
        show_vacants.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_vacantsActionPerformed(evt);
            }
        });
        hire.add(show_vacants);

        show_cvs.setText(" Cv's");
        show_cvs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                show_cvsActionPerformed(evt);
            }
        });
        hire.add(show_cvs);

        jMenuBar1.add(hire);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrograms, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrograms, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void item_signOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_item_signOutActionPerformed
        dispose();
        new Index().setVisible(true);
    }//GEN-LAST:event_item_signOutActionPerformed

    private void list_simpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list_simpleActionPerformed
       dispose();
        new User_list_simple().setVisible(true);
    }//GEN-LAST:event_list_simpleActionPerformed

    private void list_doubleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_list_doubleActionPerformed
       dispose();
        new User_list_double().setVisible(true);
    }//GEN-LAST:event_list_doubleActionPerformed

    private void show_reportsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_reportsActionPerformed
       dispose();
        new Labor_report().setVisible(true);
    }//GEN-LAST:event_show_reportsActionPerformed

    private void show_typesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_typesActionPerformed
       dispose();
        new Types().setVisible(true);
    }//GEN-LAST:event_show_typesActionPerformed

    private void show_paymentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_paymentsActionPerformed
       dispose();
        new Payments_employee().setVisible(true);
    }//GEN-LAST:event_show_paymentsActionPerformed

    private void show_vacantsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_vacantsActionPerformed
       dispose();
        new Vacant_company().setVisible(true);
    }//GEN-LAST:event_show_vacantsActionPerformed

    private void show_cvsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_show_cvsActionPerformed
       dispose();
        new Cvs_company().setVisible(true);
    }//GEN-LAST:event_show_cvsActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
       dispose();
       new Employees().setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void privileges_userActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_privileges_userActionPerformed
        dispose();
        new PrivilegesUsers().setVisible(true);
    }//GEN-LAST:event_privileges_userActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Selection_program.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Selection_program.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Selection_program.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Selection_program.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
                new Selection_program().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu file_option;
    private javax.swing.JMenu hire;
    private javax.swing.JMenuItem item_signOut;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem list_double;
    private javax.swing.JMenuItem list_simple;
    private javax.swing.JMenu new_employee;
    private org.edisoncor.gui.panel.PanelImage panelImage1;
    private javax.swing.JPanel panelPrograms;
    private javax.swing.JMenuItem privileges_user;
    private javax.swing.JMenuItem show_cvs;
    private javax.swing.JMenuItem show_payments;
    private javax.swing.JMenuItem show_reports;
    private javax.swing.JMenuItem show_types;
    private javax.swing.JMenuItem show_vacants;
    // End of variables declaration//GEN-END:variables
}
