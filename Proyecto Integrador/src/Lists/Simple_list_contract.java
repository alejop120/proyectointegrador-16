package Lists;
import Classes.Contract;
import javax.swing.*;
import Nodos.Nodo_contract;
/**
 * @author libardo
 */
public class Simple_list_contract{
  private Nodo_contract nodo;

  public Simple_list_contract() {
    this.nodo = null;
  }
  
  public boolean is_empty(){
   if(nodo == null){
    return true;  
   }
   else{
    return false;
   }
  }
  
  public void insert(Contract contract){
    Nodo_contract new_nodo = new Nodo_contract(contract);
    if(is_empty()){
     nodo = new_nodo;    
    }
    else{
      Nodo_contract temp = nodo;
      while(temp.getNext() != null){
        temp = temp.getNext();
      }
      
      temp.setNext(new_nodo);
    }
  }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_contract temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_contract temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_contract temp = nodo;
       Nodo_contract temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public Nodo_contract get(int position){
      Nodo_contract temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_contract display(int position){
      Nodo_contract temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_contract search_index(String index){
      Nodo_contract temp = nodo;
      while(temp != null){
         String pos = temp.contract.getPosition();
         String date = temp.contract.getDate();
         if(pos.equals(index) || date.equals(index)){
            return temp;
         }
         temp = temp.next;
      }
      return null;
    }
    
    public void update(String data, Contract update_contract){
      Nodo_contract temp = search_index(data);
      if(temp != null){
        temp.contract = update_contract;
      }
    }
    
   public void delete_list(String position, String date, int salary){
    if(!is_empty()){
      int pos = 0;
      boolean counter = false;
      Nodo_contract temp = nodo;
      while(counter == false){
         String posi = temp.contract.getPosition();
         String dat = temp.contract.getDate();
         int sal = temp.contract.getSalary();

         if(posi.equals(position) && dat.equals(date) && sal == salary){
           counter = true;
           delete_between_node(pos);
         }

        temp = temp.next;
        pos++;
      }
    }
   }
}
