package Lists;
import Classes.Endowment;
import javax.swing.*;
import Nodos.Nodo_endowment;
/**
 * @author libardo
 */
public class Simple_list_endowment{
  private Nodo_endowment nodo;

  public Simple_list_endowment() {
    this.nodo = null;
  }
  
  public boolean is_empty(){
   if(nodo == null){
    return true;  
   }
   else{
    return false;
   }
  }
  
  public void insert(Endowment endowment){
    Nodo_endowment new_nodo = new Nodo_endowment(endowment);
    if(is_empty()){
     nodo = new_nodo;    
    }
    else{
      Nodo_endowment temp = nodo;
      while(temp.getNext() != null){
        temp = temp.getNext();
      }
      
      temp.setNext(new_nodo);
    }
  }
  
  public void insert_initial(Endowment endowment){
   if(!is_empty()){
     Nodo_endowment new_nodo = new Nodo_endowment(endowment);
     new_nodo.setNext(nodo);
     nodo = new_nodo;
   }   
  }
  
  public void delete_list(){
    if(!is_empty()){
      nodo.setNext(null);
      nodo = null;
    }
  }
  
  public String show(){
    String text = "";
    Nodo_endowment temp = nodo;
    while(temp != null){
     text += temp.getEndowment()+"\n";
     System.out.println(text+"\n");
     temp = temp.getNext();
    }
    return text;
  }
  
    public void display(){
      Nodo_endowment temp = nodo;
      while (temp != null) {
        System.out.println(temp.getEndowment());
        temp = temp.getNext();
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void insert_last(Endowment data){
      if(!is_empty()){
        Nodo_endowment new_nodo = new Nodo_endowment(data);
        Nodo_endowment temp = nodo;
        while(temp.getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(new_nodo);
       new_nodo.setNext(null);
      }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_endowment temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_endowment temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void insert_between_node(Endowment endowment, int pos){
     if(!is_empty()){
       Nodo_endowment new_nodo = new Nodo_endowment(endowment);
       Nodo_endowment temp = nodo;
       Nodo_endowment temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
          insert_initial(endowment);
       }
       
       if(pos == size()){
         insert_last(endowment);
        }
       
       if(pos > 0 && pos < size()){
         while (pos_route != pos) {
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
         new_nodo.setNext(temp);
         temp_2.setNext(new_nodo);
       }
     }
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_endowment temp = nodo;
       Nodo_endowment temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public void search(int pos){
      Nodo_endowment temp = nodo;
      int route = 0;
      if(!is_empty()){
        if(pos == 0){
           JOptionPane.showMessageDialog(null, "The value of position is: " + pos + " and data are: " + temp.endowment);
        }
        else if(pos == size()){
           while (temp != null){
            temp = temp.next;
           }
           JOptionPane.showMessageDialog(null, "The value of position " + pos + " is:" + temp.endowment);
        }
        else if(pos > 0 & pos < size()){
          while(route != (pos - 1)){
            temp = temp.getNext();
            route++;
          }
          JOptionPane.showMessageDialog(null, "the value of position " + pos + " is:" + temp.next.endowment);
          temp.next = temp.next.next;
        }
      }
    }
    
    public Nodo_endowment search_index(int index) {
     Nodo_endowment temp = nodo;
     int size = 0;
     while(temp != null){
      if(size == index){
        return temp;
      }
     
      temp = temp.next;
       size++;
      }
     return null;
    }
    
    public Nodo_endowment get(int position){
      Nodo_endowment temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }

    public void change(int pos, Endowment new_data){
      if(!is_empty()){
        Nodo_endowment temp = nodo;
        int pos_route = 0;
        while (pos_route != pos) {
          temp = temp.next;
          pos_route++;
        }
       temp.endowment = new_data;
      }
    }
}
