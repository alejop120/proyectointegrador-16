package Lists;
import Classes.Endowment;
import Nodos.Double_endowment;
/**
 * @author libardo
 */
public class Double_list_endowment{
  private Double_endowment head;
  private Double_endowment temp;

  public Double_list_endowment(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Endowment  // 
  public void insert_head(Endowment endowment) {
    Double_endowment node_endowment = new Double_endowment(endowment);
    if(is_empty()) {
      head = node_endowment;
    }
  }
  
    public void insert_initial(Endowment endowment) {
      Double_endowment node_endowment = new Double_endowment(endowment);
      if(!is_empty()){
        node_endowment.next = head;
        head.previous = node_endowment;
        head = node_endowment;
      }
    }

    public void insert_last(Endowment endowment) {
      Double_endowment node_endowment = new Double_endowment(endowment);
      Double_endowment temp = head;
      if(is_empty()){
        insert_head(endowment);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_endowment;
       node_endowment.previous = temp;
      }
    }
    
    public void insert_position(Endowment endowment, int position){
      Double_endowment node_endowment = new Double_endowment(endowment);
      if(position == 0){
        if(head == null){
          insert_head(endowment);
        }
        else{
          insert_initial(endowment);
        }
      }
      else if(position == size()){
        insert_last(endowment);
      }
      else{
        if(position > 0 && position < size()){
          Double_endowment temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_endowment.next = temp.next;
          temp.next.previous = node_endowment;
          temp.next = node_endowment;
          temp.previous = temp;
        }
       }
    }
    
    public void update(String document, Endowment update_data){
      Double_endowment temp = search(document);
      if(temp != null){
        temp.endowment = update_data;
      }
    }
//  Class Endowment  //

    public void delete_last(){
      Double_endowment temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_endowment temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_endowment temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_endowment search(String document){
      Double_endowment temp = head;
      if(!is_empty()){
        if(document.equals(temp.endowment.getName())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.endowment.getName())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_endowment get(int position){
      Double_endowment temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_endowment temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.endowment + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_endowment temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.endowment + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
