package Lists;
import Classes.Benefit_service;
import Nodos.Nodo_benefit;
/**
 * @author libardo
 */
public class Simple_list_benefit_service{
    private Nodo_benefit nodo;

    public Simple_list_benefit_service() {
      this.nodo = null;
    }

    public boolean is_empty(){
     if(nodo == null){
      return true;  
     }
     else{
      return false;
     }
    }
  
    public void insert(Benefit_service benefit){
      Nodo_benefit new_nodo = new Nodo_benefit(benefit);
      if(is_empty()){
       nodo = new_nodo;    
      }
      else{
        Nodo_benefit temp = nodo;
        while(temp.getNext() != null){
          temp = temp.getNext();
        }

        temp.setNext(new_nodo);
      }
    }
    
    public Nodo_benefit get(int position){
      Nodo_benefit temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
     
    public Nodo_benefit display(int position){
      Nodo_benefit temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_benefit search_index(String index){
      Nodo_benefit temp = nodo;
      while(temp != null){
         String arl = temp.benefit.getArl();
         String eps = temp.benefit.getEps();
         String comp = temp.benefit.getCompensation_fund();
         String pens = temp.benefit.getPension_severance();
         if(arl.equals(index) || eps.equals(index) || comp.equals(index) || pens.equals(index)){
            return temp;
         }
         temp = temp.next;
      }
      return null;
    }
    
    public void update(String data, Benefit_service update_benefit){
      Nodo_benefit temp = search_index(data);
      if(temp != null){
        temp.benefit = update_benefit;
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_benefit temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_benefit temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
        
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_benefit temp = nodo;
       Nodo_benefit temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public void delete_list(String arl, String eps, String pension, String compensation){
     if(!is_empty()){
       int pos = 0;
       boolean counter = false;
       Nodo_benefit temp = nodo;
       while(temp != null && counter == false){
          String arl_benefit = temp.benefit.getArl();
          String eps_benefit = temp.benefit.getEps();
          String compensation_benefit = temp.benefit.getCompensation_fund();
          String pension_benefit = temp.benefit.getPension_severance();

          if(arl_benefit.equals(arl) && eps_benefit.equals(eps) && compensation_benefit.equals(compensation) && pension_benefit.equals(pension)){
            counter = true;
            delete_between_node(pos);
          }

         temp = temp.next;
         pos++;
       }
     }
    }
}
