package Lists;
import Classes.Type_contract;
import Nodos.Double_type;
/**
 * @author libardo
 */
public class Double_list_type{
  private Double_type head;
  private Double_type temp;

  public Double_list_type(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Type_contract  // 
  public void insert_head(Type_contract type) {
    Double_type node_type = new Double_type(type);
    if(is_empty()) {
      head = node_type;
    }
  }
  
    public void insert_initial(Type_contract type) {
      Double_type node_type = new Double_type(type);
      if(!is_empty()){
        node_type.next = head;
        head.previous = node_type;
        head = node_type;
      }
    }

    public void insert_last(Type_contract type) {
      Double_type node_type = new Double_type(type);
      Double_type temp = head;
      if(is_empty()){
        insert_head(type);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_type;
       node_type.previous = temp;
      }
    }
    
    public void insert_position(Type_contract type, int position){
      Double_type node_type = new Double_type(type);
      if(position == 0){
        if(head == null){
          insert_head(type);
        }
        else{
          insert_initial(type);
        }
      }
      else if(position == size()){
        insert_last(type);
      }
      else{
        if(position > 0 && position < size()){
          Double_type temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_type.next = temp.next;
          temp.next.previous = node_type;
          temp.next = node_type;
          temp.previous = temp;
        }
       }
    }
    
    public void update(String document, Type_contract update_data){
      Double_type temp = search(document);
      if(temp != null){
        temp.type = update_data;
      }
    }
//  Class Type_contract  //

    public void delete_last(){
      Double_type temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_type temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_type temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_type search(String document){
      Double_type temp = head;
      if(!is_empty()){
        if(document.equals(temp.type.getName())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.type.getName())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_type get(int position){
      Double_type temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_type temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.type + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_type temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.type + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
