package Lists;
import Classes.Payment;
import Nodos.Nodo_payment;
/**
 * @author libardo
 */
public class Simple_list_payment{
    private Nodo_payment nodo;

    public Simple_list_payment(){
      this.nodo = null;
    }

    public boolean is_empty(){
     if(nodo == null){
      return true;  
     }
     else{
      return false;
     }
    }

    public void insert(Payment payment){
      Nodo_payment new_nodo = new Nodo_payment(payment);
      if(is_empty()){
       nodo = new_nodo;    
      }
      else{
        Nodo_payment temp = nodo;
        while(temp.getNext() != null){
          temp = temp.getNext();
        }
        temp.setNext(new_nodo);
      }
    }
 
    public Nodo_payment display(int position){
      Nodo_payment temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_payment search_index(String index){
      Nodo_payment temp = nodo;
      while(temp != null){
          String conc = temp.payment.getConcept();
          String obse = temp.payment.getObservations();
          int val = temp.payment.getValue();
          String dat = temp.payment.getDate();
          boolean ref = temp.payment.getRefunded();

         if(conc.equals(index) || obse.equals(index) || val == Integer.parseInt(index) || dat.equals(index) || String.valueOf(ref).equals(index)){
            return temp;
         }
         temp = temp.next;
      }
      return null;
    }
    
    public void update(String data, Payment update_pay){
      Nodo_payment temp = search_index(data);
      if(temp != null){
        temp.payment = update_pay;
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_payment temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_payment temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_payment temp = nodo;
       Nodo_payment temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public Nodo_payment get(int position){
      Nodo_payment temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public void delete_list(String concept, String observation, int value){
     if(!is_empty()){
       int pos = 0;
       boolean counter = false;
       Nodo_payment temp = nodo;
       while(temp != null && counter == false){
          String conc = temp.payment.getConcept();
          String obse = temp.payment.getObservations();
          int val = temp.payment.getValue();

          if(conc.equals(concept) && obse.equals(observation) && val == value){
            counter = true;
            delete_between_node(pos);
          }

         temp = temp.next;
         pos++;
       }
     }
    }
}
