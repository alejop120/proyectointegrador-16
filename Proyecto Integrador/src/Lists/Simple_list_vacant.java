package Lists;
import Classes.Vacant;
import Nodos.Nodo_vacant;
/**
 * @author libardo
 */
public class Simple_list_vacant{
   private Nodo_vacant nodo;

   public Simple_list_vacant(){
     this.nodo = null;
   }
  
   public boolean is_empty(){
    if(nodo == null){
     return true;  
    }
    else{
     return false;
    }
   }
  
    public void insert(Vacant vacant){
      Nodo_vacant new_nodo = new Nodo_vacant(vacant);
      if(is_empty()){
       nodo = new_nodo;    
      }
      else{
        Nodo_vacant temp = nodo;
        while(temp.getNext() != null){
          temp = temp.getNext();
        }
       temp.setNext(new_nodo);
      }
    }
    
    public int size(){
      Nodo_vacant temp = nodo;
      int count = 0;
      if(!is_empty()){
        count++;
        while (temp.next != null){
         count++;
         temp = temp.next;
        }
      }
      return count;
    }
    
    public Nodo_vacant get(int position){
      Nodo_vacant temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_vacant display(int position){
      Nodo_vacant temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_vacant search_index(String index){
      Nodo_vacant temp = nodo;
      while(temp != null){
         String vacant_name = temp.vacant.getName();
         String vacant_pos = temp.vacant.getPosition();
         String vacant_desc = temp.vacant.getDescription();
         String vacant_area = temp.vacant.getArea();
         if(vacant_name.equals(index) || vacant_pos.equals(index) || vacant_desc.equals(index) || vacant_area.equals(index)){
            return temp;
         }
         temp = temp.next;
      }
      return null;
    }
    
    public void update(String data, Vacant update_vacant){
      Nodo_vacant temp = search_index(data);
      if(temp != null){
        temp.vacant = update_vacant;
      }
    }
  
    public void delete_initial(){
     if(!is_empty()){
       nodo = nodo.getNext();
     }
    }
   
    public void delete_last(){
      if(!is_empty()){
        Nodo_vacant temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }

    public void delete_between_node(int pos){
      if(!is_empty()){
        Nodo_vacant temp = nodo;
        Nodo_vacant temp_2 = temp;
        int pos_route = 0;
         if(pos == 0){
            delete_initial();
         }
         else if(pos == size()){
                delete_last();
         }
         else if(pos > 0 && pos < size()){
           while(pos_route != pos){
                temp_2 = temp;
                temp = temp.getNext();
                pos_route++;
           }
           temp_2.setNext(temp.getNext());
         }
      }
    }
    
    public void delete_list(String name, String position, String description, String area){
     if(!is_empty()){
       int pos = 0;
       boolean counter = false;
       Nodo_vacant temp = nodo;
       while(counter == false){
          String vacant_name = temp.vacant.getName();
          String vacant_pos = temp.vacant.getPosition();
          String vacant_desc = temp.vacant.getDescription();
          String vacant_area = temp.vacant.getArea();

          if(vacant_name.equals(name) && vacant_pos.equals(position) && vacant_desc.equals(description) && vacant_area.equals(area)){
            counter = true;
            delete_between_node(pos);
          }

         temp = temp.next;
         pos++;
       }
     }
    }
}
