package Lists;
import Classes.Cv_company;
import Nodos.Nodo_cv;
/**
 * @author libardo
 */
public class Simple_list_cv_company{
    private Nodo_cv nodo;

    public Simple_list_cv_company(){
      this.nodo = null;
    }

    public boolean is_empty(){
     if(nodo == null){
      return true;  
     }
     else{
      return false;
     }
    }
  
    public void insert(Cv_company cv){
      Nodo_cv new_nodo = new Nodo_cv(cv);
      if(is_empty()){
       nodo = new_nodo;    
      }
      else{
        Nodo_cv temp = nodo;
        while(temp.getNext() != null){
          temp = temp.getNext();
        }

        temp.setNext(new_nodo);
      }
    }
  
    public Nodo_cv display(int position){
      Nodo_cv temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public Nodo_cv search_index(String index){
      Nodo_cv temp = nodo;
      while(temp != null){
         String name = temp.cv.getName();
         String surname = temp.cv.getSurname();
         int document = temp.cv.getDocument();
         String pos = temp.cv.getPosition();
         String req = temp.cv.getRequested_position();
         if(name.equals(index) || surname.equals(index) || document == Integer.parseInt(index) || pos.equals(index) || req.equals(index)){
            return temp;
         }
         temp = temp.next;
      }
      return null;
    }
    
    public void update(String data, Cv_company update_cv){
      Nodo_cv temp = search_index(data);
      if(temp != null){
        temp.cv = update_cv;
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_cv temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_cv temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_cv temp = nodo;
       Nodo_cv temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public Nodo_cv get(int position){
      Nodo_cv temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }

    public void delete_list(String name, String surname, int document, String position, String requested){
     if(!is_empty()){
       int pos = 0;
       boolean counter = false;
       Nodo_cv temp = nodo;
       while(temp != null && counter == false){
          String name_cv = temp.cv.getName();
          String surname_cv = temp.cv.getSurname();
          int document_cv = temp.cv.getDocument();
          String pos_cv = temp.cv.getPosition();
          String req_cv = temp.cv.getRequested_position();

          if(name_cv.equals(name) && surname_cv.equals(surname) && document_cv == document && pos_cv.equals(position) && req_cv.equals(requested)){
            counter = true;
            delete_between_node(pos);
          }

         temp = temp.next;
         pos++;
       }
     }
    }
}
