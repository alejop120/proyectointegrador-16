package Lists;
import Classes.Payment;
import Nodos.Double_payment;
/**
 * @author libardo
 */
public class Double_list_payment{
  private Double_payment head;
  private Double_payment temp;

  public Double_list_payment(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Payment  // 
  public void insert_head(Payment payment) {
    Double_payment node_payment = new Double_payment(payment);
    if(is_empty()) {
      head = node_payment;
    }
  }
  
    public void insert_initial(Payment payment) {
      Double_payment node_payment = new Double_payment(payment);
      if(!is_empty()){
        node_payment.next = head;
        head.previous = node_payment;
        head = node_payment;
      }
    }

    public void insert_last(Payment payment) {
      Double_payment node_payment = new Double_payment(payment);
      Double_payment temp = head;
      if(is_empty()){
        insert_head(payment);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_payment;
       node_payment.previous = temp;
      }
    }
    
    public void insert_position(Payment payment, int position){
      Double_payment node_endowment = new Double_payment(payment);
      if(position == 0){
        if(head == null){
          insert_head(payment);
        }
        else{
          insert_initial(payment);
        }
      }
      else if(position == size()){
        insert_last(payment);
      }
      else{
        if(position > 0 && position < size()){
          Double_payment temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_endowment.next = temp.next;
          temp.next.previous = node_endowment;
          temp.next = node_endowment;
          temp.previous = temp;
        }
       }
    }
    
    public void update(String document, Payment update_data){
      Double_payment temp = search(document);
      if(temp != null){
        temp.payment = update_data;
      }
    }
//  Class Payment  //

    public void delete_last(){
      Double_payment temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_payment temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_payment temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_payment search(String document){
      Double_payment temp = head;
      if(!is_empty()){
        if(document.equals(temp.payment.getConcept())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.payment.getConcept())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_payment get(int position){
      Double_payment temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_payment temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.payment + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_payment temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.payment + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
