package Lists;
import Classes.User;
import javax.swing.*;
import Classes.Nodo;
/**
 * @author libardo sánchez
 */
public class Simple_list{
  private Nodo nodo;

  public Simple_list() {
    this.nodo = null;
  }
  
  public boolean is_empty(){
   if(nodo == null){
    return true;  
   }
   else{
    return false;
   }
  }
  
  public void insert(User data){
    Nodo new_nodo = new Nodo(data);
    if(is_empty()){
     nodo = new_nodo;    
    }
    else{
      Nodo temp = nodo;
      while(temp.getNext() != null){
        temp = temp.getNext();
      }
      
      temp.setNext(new_nodo);
    }
  }
  
  public void insert_initial(User data){
   if(!is_empty()){
     Nodo new_nodo = new Nodo(data);
     new_nodo.setNext(nodo);
     nodo = new_nodo;
   }   
  }
  
  public void delete_list(){
    if(!is_empty()){
      nodo.setNext(null);
      nodo = null;
    }
  }
  
  public String show(){
    String text = "";
    Nodo temp = nodo;
    while(temp != null){
     text += temp.getData()+"\n";
     System.out.println(text+"\n");
     temp = temp.getNext();
    }
    return text;
  }
  
    public void display(){
      Nodo temp = nodo;
      while (temp != null) {
        System.out.println(temp.getData());
        temp = temp.getNext();
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void insert_last(User data){
      if(!is_empty()){
        Nodo new_nodo = new Nodo(data);
        Nodo temp = nodo;
        while(temp.getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(new_nodo);
       new_nodo.setNext(null);
      }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void insert_between_node(User dato, int pos){
     if(!is_empty()){
       Nodo new_nodo = new Nodo(dato);
       Nodo temp = nodo;
       Nodo temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
          insert_initial(dato);
       }
       
       if(pos == size()){
         insert_last(dato);
        }
       
       if(pos > 0 && pos < size()){
         while (pos_route != pos) {
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
         new_nodo.setNext(temp);
         temp_2.setNext(new_nodo);
       }
     }
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo temp = nodo;
       Nodo temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public void search(int pos){
      Nodo temp = nodo;
      int route = 0;
      if(!is_empty()){
        if(pos == 0){
           JOptionPane.showMessageDialog(null, "The value of position is: " + pos + " and data are: " + temp.data);
        }
        else if(pos == size()){
           while (temp != null){
            temp = temp.next;
           }
           JOptionPane.showMessageDialog(null, "The value of position " + pos + " is:" + temp.data);
        }
        else if(pos > 0 & pos < size()){
          while(route != (pos - 1)){
            temp = temp.getNext();
            route++;
          }
          JOptionPane.showMessageDialog(null, "the value of position " + pos + " is:" + temp.next.data);
          temp.next = temp.next.next;
        }
      }
    }
    
    public Nodo search_index(int index) {
     Nodo temp = nodo;
     int size = 0;
     while(temp != null){
      if(size == index){
        return temp;
      }
     
      temp = temp.next;
       size++;
      }
     return null;
    }
    
    public Nodo get(int position){
      Nodo temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }

    public void change(int pos, User new_data){
      if(!is_empty()){
        Nodo temp = nodo;
        int pos_route = 0;
        while (pos_route != pos) {
          temp = temp.next;
          pos_route++;
        }
       temp.data = new_data;
      }
    }
}
