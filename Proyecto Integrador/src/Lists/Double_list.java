package Lists;
import Classes.User;
import Classes.Nodo_doble;
/**
 * @author libardo sánchez
 */
public class Double_list{
  private Nodo_doble head;
  private Nodo_doble temp;

  public Double_list(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class User  // 
  public void insert_head(User data) {
    Nodo_doble new_node = new Nodo_doble(data);
    if(is_empty()) {
      head = new_node;
    }
  }
  
    public void insert_initial(User data) {
      Nodo_doble new_node = new Nodo_doble(data);
      if(!is_empty()){
        new_node.next = head;
        head.previous = new_node;
        head = new_node;
      }
    }

    public void insert_last(User data) {
      Nodo_doble new_node = new Nodo_doble(data);
      Nodo_doble temp = head;
      if(is_empty()){
        insert_head(data);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = new_node;
       new_node.previous = temp;
      }
    }
    
    public void insert_position(User data, int position){
      Nodo_doble new_node = new Nodo_doble(data);
      if(position == 0){
        if(head == null){
          insert_head(data);
        }
        else{
          insert_initial(data);
        }
      }
      else if(position == size()){
        insert_last(data);
      }
      else{
        if(position > 0 && position < size()){
          Nodo_doble temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          new_node.next = temp.next;
          temp.next.previous = new_node;
          temp.next = new_node;
          temp.previous = temp;
        }
       }
    }
    
    public void update(String document, User update_data){
      Nodo_doble temp = search(document);
      if(temp != null){
        temp.data = update_data;
      }
    }
//  Class User  //

    public void delete_last(){
      Nodo_doble temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Nodo_doble temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Nodo_doble temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Nodo_doble search(String document){
      Nodo_doble temp = head;
      if(!is_empty()){
        if(temp.data.getDocument().equals(document)){
          return temp;
        }
      }
      while(temp.next != null){
        if(temp.next.data.getDocument().equals(document)){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Nodo_doble get(int position){
      Nodo_doble temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Nodo_doble temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.data + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Nodo_doble temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.data + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
