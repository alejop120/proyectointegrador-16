package Lists;
import Classes.Benefit_service;
import Nodos.Double_benefit;
/**
 * @author libardo
 */
public class Double_list_benefit_service{
   private Double_benefit head;
  private Double_benefit temp;

  public Double_list_benefit_service(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Benefit service  // 
  public void insert_head_benefit(Benefit_service benefit) {
    Double_benefit node_benefit = new Double_benefit(benefit);
    if(is_empty()){
      head = node_benefit;
    }
  }
  
    public void insert_initial_benefit(Benefit_service benefit) {
      Double_benefit node_benefit = new Double_benefit(benefit);
      if(!is_empty()){
        node_benefit.next = head;
        head.previous = node_benefit;
        head = node_benefit;
      }
    }

    public void insert_last_benefit(Benefit_service benefit) {
      Double_benefit node_benefit = new Double_benefit(benefit);
      Double_benefit temp = head;
      if(is_empty()){
        insert_head_benefit(benefit);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_benefit;
       node_benefit.previous = temp;
      }
    }
    
    public void insert_position_benefit(Benefit_service benefit, int position){
      Double_benefit node_benefit = new Double_benefit(benefit);
      if(position == 0){
        if(head == null){
          insert_head_benefit(benefit);
        }
        else{
          insert_initial_benefit(benefit);
        }
      }
      else if(position == size()){
        insert_last_benefit(benefit);
      }
      else{
        if(position > 0 && position < size()){
          Double_benefit temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_benefit.next = temp.next;
          temp.next.previous = node_benefit;
          temp.next = node_benefit;
          temp.previous = temp;
        }
       }
    }
    
    public void update_benefit(String document, Benefit_service update_data){
      Double_benefit temp = search(document);
      if(temp != null){
        temp.benefit = update_data;
      }
    }
//  Class Benefit service  //
    

    public void delete_last(){
      Double_benefit temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_benefit temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_benefit temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_benefit search(String document){
      Double_benefit temp = head;
      if(!is_empty()){
        if(temp.benefit.getArl().equals(document)){
          return temp;
        }
      }
      while(temp.next != null){
        if(temp.next.benefit.getArl().equals(document)){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_benefit get(int position){
      Double_benefit temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_benefit temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.benefit + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_benefit temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.benefit + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
