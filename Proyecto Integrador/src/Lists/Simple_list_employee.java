package Lists;
import Classes.Employee;
import javax.swing.*;
import Nodos.Nodo_employee;
/**
 * @author libardo
 */
public class Simple_list_employee{
    private Nodo_employee nodo;

  public Simple_list_employee(){
    this.nodo = null;
  }
  
  public boolean is_empty(){
   if(nodo == null){
    return true;  
   }
   else{
    return false;
   }
  }
  
  public void insert(Employee employee){
    Nodo_employee new_nodo = new Nodo_employee(employee);
    if(is_empty()){
     nodo = new_nodo;    
    }
    else{
      Nodo_employee temp = nodo;
      while(temp.getNext() != null){
        temp = temp.getNext();
      }
      
      temp.setNext(new_nodo);
    }
  }
  
  public void insert_initial(Employee employee){
   if(!is_empty()){
     Nodo_employee new_nodo = new Nodo_employee(employee);
     new_nodo.setNext(nodo);
     nodo = new_nodo;
   }   
  }
  
  public void delete_list(){
    if(!is_empty()){
      nodo.setNext(null);
      nodo = null;
    }
  }
  
  public String show(){
    String text = "";
    Nodo_employee temp = nodo;
    while(temp != null){
     text += temp.getEmployee()+"\n";
     System.out.println(text+"\n");
     temp = temp.getNext();
    }
    return text;
  }
  
    public void display(){
      Nodo_employee temp = nodo;
      while (temp != null) {
        System.out.println(temp.getEmployee());
        temp = temp.getNext();
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void insert_last(Employee data){
      if(!is_empty()){
        Nodo_employee new_nodo = new Nodo_employee(data);
        Nodo_employee temp = nodo;
        while(temp.getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(new_nodo);
       new_nodo.setNext(null);
      }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_employee temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_employee temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void insert_between_node(Employee employee, int pos){
     if(!is_empty()){
       Nodo_employee new_nodo = new Nodo_employee(employee);
       Nodo_employee temp = nodo;
       Nodo_employee temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
          insert_initial(employee);
       }
       
       if(pos == size()){
         insert_last(employee);
        }
       
       if(pos > 0 && pos < size()){
         while (pos_route != pos) {
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
         new_nodo.setNext(temp);
         temp_2.setNext(new_nodo);
       }
     }
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_employee temp = nodo;
       Nodo_employee temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public void search(int pos){
      Nodo_employee temp = nodo;
      int route = 0;
      if(!is_empty()){
        if(pos == 0){
           JOptionPane.showMessageDialog(null, "The value of position is: " + pos + " and data are: " + temp.employee);
        }
        else if(pos == size()){
           while (temp != null){
            temp = temp.next;
           }
           JOptionPane.showMessageDialog(null, "The value of position " + pos + " is:" + temp.employee);
        }
        else if(pos > 0 & pos < size()){
          while(route != (pos - 1)){
            temp = temp.getNext();
            route++;
          }
          JOptionPane.showMessageDialog(null, "the value of position " + pos + " is:" + temp.next.employee);
          temp.next = temp.next.next;
        }
      }
    }
    
    public Nodo_employee search_index(int index) {
     Nodo_employee temp = nodo;
     int size = 0;
     while(temp != null){
      if(size == index){
        return temp;
      }
     
      temp = temp.next;
       size++;
      }
     return null;
    }
    
    public Nodo_employee get(int position){
      Nodo_employee temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }

    public void change(int pos, Employee new_data){
      if(!is_empty()){
        Nodo_employee temp = nodo;
        int pos_route = 0;
        while (pos_route != pos) {
          temp = temp.next;
          pos_route++;
        }
       temp.employee = new_data;
      }
    }
}
