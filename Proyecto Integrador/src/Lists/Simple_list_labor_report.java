package Lists;
import Classes.Labor_report;
import javax.swing.*;
import Nodos.Nodo_labor;
/**
 * @author libardo
 */
public class Simple_list_labor_report {
  private Nodo_labor nodo;

  public Simple_list_labor_report() {
    this.nodo = null;
  }
  
  public boolean is_empty(){
   if(nodo == null){
    return true;  
   }
   else{
    return false;
   }
  }
  
  public void insert(Labor_report labor){
    Nodo_labor new_nodo = new Nodo_labor(labor);
    if(is_empty()){
     nodo = new_nodo;    
    }
    else{
      Nodo_labor temp = nodo;
      while(temp.getNext() != null){
        temp = temp.getNext();
      }
      
      temp.setNext(new_nodo);
    }
  }
  
  public void insert_initial(Labor_report labor){
   if(!is_empty()){
     Nodo_labor new_nodo = new Nodo_labor(labor);
     new_nodo.setNext(nodo);
     nodo = new_nodo;
   }   
  }
  
  public void delete_list(){
    if(!is_empty()){
      nodo.setNext(null);
      nodo = null;
    }
  }
  
  public String show(){
    String text = "";
    Nodo_labor temp = nodo;
    while(temp != null){
     text += temp.getLabor()+"\n";
     System.out.println(text+"\n");
     temp = temp.getNext();
    }
    return text;
  }
  
    public void display(){
      Nodo_labor temp = nodo;
      while (temp != null) {
        System.out.println(temp.getLabor());
        temp = temp.getNext();
      }
    }
    
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void insert_last(Labor_report data){
      if(!is_empty()){
        Nodo_labor new_nodo = new Nodo_labor(data);
        Nodo_labor temp = nodo;
        while(temp.getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(new_nodo);
       new_nodo.setNext(null);
      }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_labor temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public int size(){
     Nodo_labor temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }
    
    public void insert_between_node(Labor_report labor, int pos){
     if(!is_empty()){
       Nodo_labor new_nodo = new Nodo_labor(labor);
       Nodo_labor temp = nodo;
       Nodo_labor temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
          insert_initial(labor);
       }
       
       if(pos == size()){
         insert_last(labor);
        }
       
       if(pos > 0 && pos < size()){
         while (pos_route != pos) {
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
         new_nodo.setNext(temp);
         temp_2.setNext(new_nodo);
       }
     }
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_labor temp = nodo;
       Nodo_labor temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }
    
    public void search(int pos){
      Nodo_labor temp = nodo;
      int route = 0;
      if(!is_empty()){
        if(pos == 0){
           JOptionPane.showMessageDialog(null, "The value of position is: " + pos + " and data are: " + temp.labor);
        }
        else if(pos == size()){
           while (temp != null){
            temp = temp.next;
           }
           JOptionPane.showMessageDialog(null, "The value of position " + pos + " is:" + temp.labor);
        }
        else if(pos > 0 & pos < size()){
          while(route != (pos - 1)){
            temp = temp.getNext();
            route++;
          }
          JOptionPane.showMessageDialog(null, "the value of position " + pos + " is:" + temp.next.labor);
          temp.next = temp.next.next;
        }
      }
    }
    
    public Nodo_labor search_index(int index) {
     Nodo_labor temp = nodo;
     int size = 0;
     while(temp != null){
      if(size == index){
        return temp;
      }
     
      temp = temp.next;
       size++;
      }
     return null;
    }
    
    public Nodo_labor get(int position){
      Nodo_labor temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }

    public void change(int pos, Labor_report new_data){
      if(!is_empty()){
        Nodo_labor temp = nodo;
        int pos_route = 0;
        while (pos_route != pos) {
          temp = temp.next;
          pos_route++;
        }
       temp.labor = new_data;
      }
    }
}
