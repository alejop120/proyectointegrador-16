package Lists;
import Classes.Vacant;
import Nodos.Double_vacant;
/**
 * @author libardo
 */
public class Double_list_vacant{
  private Double_vacant head;
  private Double_vacant temp;

  public Double_list_vacant(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Vacant  // 
  public void insert_head(Vacant vacant) {
    Double_vacant node_type = new Double_vacant(vacant);
    if(is_empty()) {
      head = node_type;
    }
  }
  
    public void insert_initial(Vacant vacant) {
      Double_vacant node_type = new Double_vacant(vacant);
      if(!is_empty()){
        node_type.next = head;
        head.previous = node_type;
        head = node_type;
      }
    }

    public void insert_last(Vacant vacant) {
      Double_vacant node_type = new Double_vacant(vacant);
      Double_vacant temp = head;
      if(is_empty()){
        insert_head(vacant);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_type;
       node_type.previous = temp;
      }
    }
    
    public void insert_position(Vacant vacant, int position){
      Double_vacant node_type = new Double_vacant(vacant);
      if(position == 0){
        if(head == null){
          insert_head(vacant);
        }
        else{
          insert_initial(vacant);
        }
      }
      else if(position == size()){
        insert_last(vacant);
      }
      else{
        if(position > 0 && position < size()){
          Double_vacant temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_type.next = temp.next;
          temp.next.previous = node_type;
          temp.next = node_type;
          temp.previous = temp;
        }
       }
    }
    
    public void update(String document, Vacant update_data){
      Double_vacant temp = search(document);
      if(temp != null){
        temp.vacant = update_data;
      }
    }
//  Class Vacant  //

    public void delete_last(){
      Double_vacant temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_vacant temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_vacant temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_vacant search(String document){
      Double_vacant temp = head;
      if(!is_empty()){
        if(document.equals(temp.vacant.getName())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.vacant.getName())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_vacant get(int position){
      Double_vacant temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_vacant temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.vacant + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_vacant temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.vacant + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
