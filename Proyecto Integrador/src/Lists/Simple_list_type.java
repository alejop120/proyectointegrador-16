package Lists;
import Classes.Type_contract;
import Nodos.Nodo_type;
/**
 * @author libardo
 */
public class Simple_list_type{
  private Nodo_type nodo;

  public Simple_list_type() {
    this.nodo = null;
  }
  
  public boolean is_empty(){
   if(nodo == null){
    return true;  
   }
   else{
    return false;
   }
  }
  
  public void insert(Type_contract type){
    Nodo_type new_nodo = new Nodo_type(type);
    if(is_empty()){
     nodo = new_nodo;    
    }
    else{
      Nodo_type temp = nodo;
      while(temp.getNext() != null){
        temp = temp.getNext();
      }
      
      temp.setNext(new_nodo);
    }
  }
  
    public void delete_initial(){
     if (!is_empty()){
       nodo = nodo.getNext();
     }
    }
    
    public void delete_last(){
     if(!is_empty()){
        Nodo_type temp = nodo;
        while(temp.getNext().getNext() != null){
         temp = temp.getNext();
        }
       temp.setNext(null);
      }
    }
    
    public void delete_between_node(int pos){
     if(!is_empty()){
       Nodo_type temp = nodo;
       Nodo_type temp_2 = temp;
       int pos_route = 0;
       if(pos == 0){
         delete_initial();
       }
       else if(pos == size()){
         delete_last();
       }
       else if(pos > 0 && pos < size()){
         while(pos_route != pos){
          temp_2 = temp;
          temp = temp.getNext();
          pos_route++;
         }
        temp_2.setNext(temp.getNext());
       }
     }
    }

   public void delete_list(String name, String description){
     if(!is_empty()){
       int pos = 0;
       boolean counter = false;
       Nodo_type temp = nodo;
       while(counter == false){
          String type_name = temp.type.getName();
          String type_desc = temp.type.getDescription();
 
          if(type_name.equals(name) && type_desc.equals(description) ){
            counter = true;
            delete_between_node(pos);
          }

         temp = temp.next;
         pos++;
       }
     }
   }

    public Nodo_type display(int position){
      int counter = 0;
      Nodo_type temp = nodo;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
    
    public int size(){
     Nodo_type temp = nodo;
     int count = 0;
     if(!is_empty()){
       count++;
       while (temp.next != null) {
        count++;
        temp = temp.next;
       }
     }
     return count;
    }

    public Nodo_type search_index(String index){
      Nodo_type temp = nodo;
      while(temp != null){
         String type_name = temp.type.getName();
         String type_desc = temp.type.getDescription();
         if(type_name.equals(index) || type_desc.equals(index)){
            return temp;
         }
         temp = temp.next;
      }
      return null;
    }
    
    public void update(String data, Type_contract update_type){
      Nodo_type temp = search_index(data);
      if(temp != null){
        temp.type = update_type;
      }
    }
    
    public Nodo_type get(int position){
      Nodo_type temp = nodo;
      int counter = 0;
      while(counter != position){
        temp = temp.getNext();
        counter++;
      }
     return temp;
    }
}
