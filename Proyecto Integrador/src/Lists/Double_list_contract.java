package Lists;
import Classes.Contract;
import Nodos.Double_contract;
/**
 * @author libardo
 */
public class Double_list_contract{
  private Double_contract head;
  private Double_contract temp;

  public Double_list_contract(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Contract  // 
  public void insert_head_contract(Contract contract) {
    Double_contract node_contract = new Double_contract(contract);
    if(is_empty()){
      head = node_contract;
    }
  }
  
    public void insert_initial_contract(Contract contract) {
      Double_contract node_contract = new Double_contract(contract);
      if(!is_empty()){
        node_contract.next = head;
        head.previous = node_contract;
        head = node_contract;
      }
    }

    public void insert_last_contract(Contract contract) {
      Double_contract node_contract = new Double_contract(contract);
      Double_contract temp = head;
      if(is_empty()){
        insert_head_contract(contract);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_contract;
       node_contract.previous = temp;
      }
    }
    
    public void insert_position_contract(Contract contract, int position){
      Double_contract node_contract = new Double_contract(contract);
      if(position == 0){
        if(head == null){
          insert_head_contract(contract);
        }
        else{
          insert_initial_contract(contract);
        }
      }
      else if(position == size()){
        insert_last_contract(contract);
      }
      else{
        if(position > 0 && position < size()){
          Double_contract temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_contract.next = temp.next;
          temp.next.previous = node_contract;
          temp.next = node_contract;
          temp.previous = temp;
        }
       }
    }
    
    public void update_contract(String document, Contract update_data){
      Double_contract temp = search(document);
      if(temp != null){
        temp.contract = update_data;
      }
    }
//  Class Contract  //

    public void delete_last(){
      Double_contract temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_contract temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_contract temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_contract search(String document){
      Double_contract temp = head;
      if(!is_empty()){
        if(temp.contract.getPosition().equals(document)){
          return temp;
        }
      }
      while(temp.next != null){
        if(temp.next.contract.getPosition().equals(document)){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_contract get(int position){
      Double_contract temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_contract temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.contract + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_contract temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.contract + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
