package Lists;
import Classes.Labor_report;
import Nodos.Double_labor;
/**
 * @author libardo
 */
public class Double_list_labor_report{
  private Double_labor head;
  private Double_labor temp;

  public Double_list_labor_report(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Labor  // 
  public void insert_head(Labor_report labor) {
    Double_labor node_labor = new Double_labor(labor);
    if(is_empty()) {
      head = node_labor;
    }
  }
  
    public void insert_initial(Labor_report labor) {
      Double_labor node_labor = new Double_labor(labor);
      if(!is_empty()){
        node_labor.next = head;
        head.previous = node_labor;
        head = node_labor;
      }
    }

    public void insert_last(Labor_report labor) {
      Double_labor node_labor = new Double_labor(labor);
      Double_labor temp = head;
      if(is_empty()){
        insert_head(labor);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_labor;
       node_labor.previous = temp;
      }
    }
    
    public void insert_position_employee(Labor_report labor, int position){
      Double_labor node_labor = new Double_labor(labor);
      if(position == 0){
        if(head == null){
          insert_head(labor);
        }
        else{
          insert_initial(labor);
        }
      }
      else if(position == size()){
        insert_last(labor);
      }
      else{
        if(position > 0 && position < size()){
          Double_labor temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_labor.next = temp.next;
          temp.next.previous = node_labor;
          temp.next = node_labor;
          temp.previous = temp;
        }
       }
    }
    
    public void update_employee(String document, Labor_report update_data){
      Double_labor temp = search(document);
      if(temp != null){
        temp.labor = update_data;
      }
    }
//  Class Labor  //

    public void delete_last(){
      Double_labor temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_labor temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_labor temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_labor search(String document){
      Double_labor temp = head;
      if(!is_empty()){
        if(document.equals(temp.labor.getEmployee_id())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.labor.getEmployee_id())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_labor get(int position){
      Double_labor temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_labor temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.labor + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_labor temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.labor + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
