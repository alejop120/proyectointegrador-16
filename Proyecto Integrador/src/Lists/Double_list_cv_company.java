package Lists;
import Classes.Cv_company;
import Nodos.Double_cv;
/**
 * @author libardo
 */
public class Double_list_cv_company{
  private Double_cv head;
  private Double_cv temp;

  public Double_list_cv_company(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Cv  // 
  public void insert_head(Cv_company cv) {
    Double_cv node_cv = new Double_cv(cv);
    if(is_empty()) {
      head = node_cv;
    }
  }
  
    public void insert_initial(Cv_company cv) {
      Double_cv node_cv = new Double_cv(cv);
      if(!is_empty()){
        node_cv.next = head;
        head.previous = node_cv;
        head = node_cv;
      }
    }

    public void insert_last(Cv_company cv) {
      Double_cv node_cv = new Double_cv(cv);
      Double_cv temp = head;
      if(is_empty()){
        insert_head(cv);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_cv;
       node_cv.previous = temp;
      }
    }
    
    public void insert_position(Cv_company cv, int position){
      Double_cv node_cv = new Double_cv(cv);
      if(position == 0){
        if(head == null){
          insert_head(cv);
        }
        else{
          insert_initial(cv);
        }
      }
      else if(position == size()){
        insert_last(cv);
      }
      else{
        if(position > 0 && position < size()){
          Double_cv temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_cv.next = temp.next;
          temp.next.previous = node_cv;
          temp.next = node_cv;
          temp.previous = temp;
        }
       }
    }
    
    public void update_employee(String document, Cv_company update_data){
      Double_cv temp = search(document);
      if(temp != null){
        temp.cv_company = update_data;
      }
    }
//  Class Cv  //

    public void delete_last(){
      Double_cv temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_cv temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_cv temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_cv search(String document){
      Double_cv temp = head;
      if(!is_empty()){
        if(document.equals(temp.cv_company.getDocument())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.cv_company.getDocument())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_cv get(int position){
      Double_cv temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_cv temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.cv_company + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_cv temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.cv_company + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
