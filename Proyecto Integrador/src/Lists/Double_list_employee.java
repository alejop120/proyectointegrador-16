package Lists;
import Classes.Employee;
import Nodos.Double_employee;
/**
 * @author libardo
 */
public class Double_list_employee{
  private Double_employee head;
  private Double_employee temp;

  public Double_list_employee(){
    head = null;
  }

  public boolean is_empty(){
    return (this.head == null);
  }

//  Class Employee  // 
  public void insert_head_employee(Employee employee) {
    Double_employee node_employee = new Double_employee(employee);
    if(is_empty()) {
      head = node_employee;
    }
  }
  
    public void insert_initial_employee(Employee employee) {
      Double_employee node_employee = new Double_employee(employee);
      if(!is_empty()){
        node_employee.next = head;
        head.previous = node_employee;
        head = node_employee;
      }
    }

    public void insert_last_employee(Employee employee) {
      Double_employee node_employee = new Double_employee(employee);
      Double_employee temp = head;
      if(is_empty()){
        insert_head_employee(employee);
      }
      else{
        while(temp.next != null){
          temp = temp.next;
        }
       temp.next = node_employee;
       node_employee.previous = temp;
      }
    }
    
    public void insert_position_employee(Employee employee, int position){
      Double_employee node_employee = new Double_employee(employee);
      if(position == 0){
        if(head == null){
          insert_head_employee(employee);
        }
        else{
          insert_initial_employee(employee);
        }
      }
      else if(position == size()){
        insert_last_employee(employee);
      }
      else{
        if(position > 0 && position < size()){
          Double_employee temp = head;
          int route = 1;
          while(route != position){
            temp = temp.next;
            route++;
          }
          node_employee.next = temp.next;
          temp.next.previous = node_employee;
          temp.next = node_employee;
          temp.previous = temp;
        }
       }
    }
    
    public void update_employee(String document, Employee update_data){
      Double_employee temp = search(document);
      if(temp != null){
        temp.employee = update_data;
      }
    }
//  Class Employee  //

    public void delete_last(){
      Double_employee temp = head;
      while(temp.next.next != null){
        temp = temp.next;
      }
      temp.next = null;
    }
    
    public void delete_initial(){
      if(!is_empty()){
        if(head.next != null){
          head = head.next;
        }
      }
    }

    public void delete(int position){
      Double_employee temp = head;
      int pos = 1;
      if(!is_empty()){
        if(position == 1){
          delete_initial();
        }
        else if (position == size()){
         delete_last();
        }
        else if(position > 0 && position < size()){
          while (pos != position){
            temp = temp.next;
            pos++;
          }
          temp.previous.next = temp.next;
          temp.next.previous = temp.previous;
          temp.next = null;
          temp.previous = null;
        }
       }
    }
    
    public int size(){
      int counter = 0;
      Double_employee temp = head;
      if(!is_empty()){
        while (temp != null){
         temp = temp.next;
         counter++;
        }
      }
     return counter;
    }

    public Double_employee search(String document){
      Double_employee temp = head;
      if(!is_empty()){
        if(document.equals(temp.employee.getDocumentNumber())){
          return temp;
        }
      }
      while(temp.next != null){
        if(document.equals(temp.next.employee.getDocumentNumber())){
         return temp.next;
        }
        temp = temp.next;
       }
      return null;
    }
    
    public Double_employee get(int position){
      Double_employee temp = head;
      int counter = 0;
      while (counter != position){
        temp = temp.next;
        counter++;
      }
     return temp;
    }

    public void print(){
      Double_employee temp = head;
      if(!is_empty()){
        while (temp != null) {
          System.out.println("[" + temp.employee + "]");
          temp = temp.next;
        }
      } 
      else{
        System.out.println("The list is empty.");
      }
    }

    public String read(){
      String a = "";
      Double_employee temp_2 = head;
      if(!is_empty()){
        while (temp_2 != null){
         a = a + "[" + temp_2.employee + "]" + "\n";
         temp_2 = temp_2.next;
        }
      }
      else{
        System.out.println("The list is empty.");
      }
     return a;
    }
}
