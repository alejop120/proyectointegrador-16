package Nodos;
import Classes.Type_contract;
/**
 * @author libardo
 */
public class Nodo_type{
  public Type_contract type;
  
  public Nodo_type next;
  public int key;

//  Nodo Type_contract contract  //
  public Nodo_type(Type_contract type){
   this.type = type;
   next = null;
  }
  
  public Nodo_type(Type_contract type, int key){
    this.type = type;
    this.key = key;
    next = null;
  }
  
  public Object getType(){
    return type;
  }

  public void setType(Type_contract type){
    this.type = type;
  }

  public Nodo_type getNext(){
    return next;
  }

  public void setNext(Nodo_type next){
    this.next = next;
  } 
//  Nodo type contract  //  
}
