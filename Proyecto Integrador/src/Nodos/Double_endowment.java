package Nodos;
import Classes.Endowment;
/**
 * @author libardo
 */
public class Double_endowment{
  public Endowment endowment;
  public Double_endowment next;
  public Double_endowment previous;
   
   public Double_endowment(Endowment endowment){
    this.endowment = endowment;
    next = null;
    previous = null;
   } 
}
