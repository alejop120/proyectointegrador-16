package Nodos;
import Classes.Contract;
/**
 * @author libardo
 */
public class Nodo_contract{
  public Contract contract;
  
  public Nodo_contract next;
  public int key;

  //  Nodo Contract  //
  public Nodo_contract(Contract contract){
   this.contract = contract;
   next = null;
  }
  
  public Nodo_contract(Contract contract, int key){
    this.contract = contract;
    this.key = key;
    next = null;
  }
  
  public Object getContract(){
    return contract;
  }

  public void setContract(Contract contract){
    this.contract = contract;
  }

  public Nodo_contract getNext(){
    return next;
  }

  public void setNext(Nodo_contract next){
    this.next = next;
  } 
//  Nodo Contract  // 
}
