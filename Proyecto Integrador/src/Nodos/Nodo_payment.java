package Nodos;
import Classes.Payment;
/**
 * @author libardo
 */
public class Nodo_payment{
  public Payment payment;
  
  public Nodo_payment next;
  public int key;

//  Nodo Payment  //
  public Nodo_payment(Payment payment){
   this.payment = payment;
   next = null;
  }
  
  public Nodo_payment(Payment payment, int key){
    this.payment = payment;
    this.key = key;
    next = null;
  }
  
  public Object getPayment(){
    return payment;
  }

  public void setPayment(Payment payment){
    this.payment = payment;
  }

  public Nodo_payment getNext(){
    return next;
  }

  public void setNext(Nodo_payment next){
    this.next = next;
  } 
//  Nodo Payment  //       
}
