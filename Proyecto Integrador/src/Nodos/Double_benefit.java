package Nodos;
import Classes.Benefit_service;
/**
 * @author libardo
 */
public class Double_benefit{
  public Benefit_service benefit;
  public Double_benefit next;
  public Double_benefit previous;

  public Double_benefit(Benefit_service benefit){
   this.benefit = benefit;
   next = null;
   previous = null;
  }    
}
