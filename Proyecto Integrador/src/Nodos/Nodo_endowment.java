package Nodos;
import Classes.Endowment;
/**
 * @author libardo
 */
public class Nodo_endowment{
  public Endowment endowment;
  
  public Nodo_endowment next;
  public int key;

  // Nodo Endowment  //
  public Nodo_endowment(Endowment endowment){
   this.endowment = endowment;
   next = null;
  }
  
  public Nodo_endowment(Endowment endowment, int key){
    this.endowment = endowment;
    this.key = key;
    next = null;
  }
  
  public Object getEndowment(){
    return endowment;
  }

  public void setEndowment(Endowment endowment){
    this.endowment = endowment;
  }

  public Nodo_endowment getNext(){
    return next;
  }

  public void setNext(Nodo_endowment next){
    this.next = next;
  } 
//  Nodo Endowment  //
}
