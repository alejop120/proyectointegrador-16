package Nodos;
import Classes.Payment;
/**
 * @author libardo
 */
public class Double_payment{
  public Payment payment;
  public Double_payment next;
  public Double_payment previous;
   
   public Double_payment(Payment payment){
    this.payment = payment;
    next = null;
    previous = null;
   } 
}
