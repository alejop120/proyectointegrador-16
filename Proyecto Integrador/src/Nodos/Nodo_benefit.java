package Nodos;
import Classes.Benefit_service;
/**
 * @author libardo
 */
public class Nodo_benefit{
  public Benefit_service benefit;
  
  public Nodo_benefit next;
  public int key;

  // Nodo benefit service  //
  public Nodo_benefit(Benefit_service benefit){
   this.benefit = benefit;
   next = null;
  }
  
  public Nodo_benefit(Benefit_service benefit, int key){
    this.benefit = benefit;
    this.key = key;
    next = null;
  }
  
  public Object getBenefit(){
    return benefit;
  }

  public void setEmployee(Benefit_service benefit){
    this.benefit = benefit;
  }

  public Nodo_benefit getNext(){
    return next;
  }

  public void setNext(Nodo_benefit next){
    this.next = next;
  } 
// Nodo Benefit service  // 
}
