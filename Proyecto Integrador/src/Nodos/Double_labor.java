package Nodos;
import Classes.Labor_report;
/**
 * @author libardo
 */
public class Double_labor{
  public Labor_report labor;
  public Double_labor next;
  public Double_labor previous;
   
   public Double_labor(Labor_report labor){
    this.labor = labor;
    next = null;
    previous = null;
   } 
}
