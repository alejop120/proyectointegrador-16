package Nodos;
import Classes.Vacant;
/**
 * @author libardo
 */
public class Double_vacant{
  public Vacant vacant;
  public Double_vacant next;
  public Double_vacant previous;
   
   public Double_vacant(Vacant vacant){
    this.vacant = vacant;
    next = null;
    previous = null;
   } 
}
