package Nodos;
import Classes.Employee;
/**
 * @author libardo
 */
public class Nodo_employee{
  public Employee employee;
  
  public Nodo_employee next;
  public int key;

// Nodo Employee  //
  public Nodo_employee(Employee employee){
   this.employee = employee;
   next = null;
  }
  
  public Nodo_employee(Employee employee, int key){
    this.employee = employee;
    this.key = key;
    next = null;
  }
  
  public Object getEmployee(){
    return employee;
  }

  public void setEmployee(Employee employee){
    this.employee = employee;
  }

  public Nodo_employee getNext(){
    return next;
  }

  public void setNext(Nodo_employee next){
    this.next = next;
  } 
// Nodo Employee  //
}
