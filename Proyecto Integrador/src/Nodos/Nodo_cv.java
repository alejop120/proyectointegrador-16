package Nodos;
import Classes.Cv_company;
/**
 * @author libardo
 */
public class Nodo_cv{
  public Cv_company cv;
  
  public Nodo_cv next;
  public int key;

  //  Nodo CV company  //
  public Nodo_cv(Cv_company cv){
   this.cv = cv;
   next = null;
  }
  
  public Nodo_cv(Cv_company cv, int key){
    this.cv = cv;
    this.key = key;
    next = null;
  }
  
  public Object getCv(){
    return cv;
  }

  public void setCv(Cv_company cv){
    this.cv = cv;
  }

  public Nodo_cv getNext(){
    return next;
  }

  public void setNext(Nodo_cv next){
    this.next = next;
  } 
//  Nodo Cv company  // 
}
