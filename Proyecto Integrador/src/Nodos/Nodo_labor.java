package Nodos;
import Classes.Labor_report;
/**
 * @author libardo
 */
public class Nodo_labor{
  public Labor_report labor;
  
  public Nodo_labor next;
  public int key;

//  Nodo Labor Report  //
  public Nodo_labor(Labor_report labor){
   this.labor = labor;
   next = null;
  }
  
  public Nodo_labor(Labor_report labor, int key){
    this.labor = labor;
    this.key = key;
    next = null;
  }
  
  public Object getLabor(){
    return labor;
  }

  public void setLabor(Labor_report labor){
    this.labor = labor;
  }

  public Nodo_labor getNext(){
    return next;
  }

  public void setNext(Nodo_labor next){
    this.next = next;
  } 
//  Nodo Labor Report  //   
}
