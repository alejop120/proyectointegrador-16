package Nodos;
import Classes.Employee;
/**
 * @author libardo
 */
public class Double_employee{
  public Employee employee;
  public Double_employee next;
  public Double_employee previous;
   
   public Double_employee(Employee employee){
    this.employee = employee;
    next = null;
    previous = null;
   } 
}
