package Nodos;
import Classes.Contract;
/**
 * @author libardo
 */
public class Double_contract{
  public Contract contract;
  public Double_contract next;
  public Double_contract previous;

   public Double_contract(Contract contract){
    this.contract = contract;
    next = null;
    previous = null;
   }  
}
