package Nodos;
import Classes.Type_contract;
/**
 * @author libardo
 */
public class Double_type{
  public Type_contract type;
  public Double_type next;
  public Double_type previous;
   
   public Double_type(Type_contract type){
    this.type = type;
    next = null;
    previous = null;
   } 
}
