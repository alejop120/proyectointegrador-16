package Nodos;
import Classes.Vacant;
/**
 * @author libardo
 */
public class Nodo_vacant{
  public Vacant vacant;
  
  public Nodo_vacant next;
  public int key;

//  Nodo Vacant  //
  public Nodo_vacant(Vacant vacant){
   this.vacant = vacant;
   next = null;
  }
  
  public Nodo_vacant(Vacant vacant, int key){
    this.vacant = vacant;
    this.key = key;
    next = null;
  }
  
  public Object getVacant(){
    return vacant;
  }

  public void setVacant(Vacant vacant){
    this.vacant = vacant;
  }

  public Nodo_vacant getNext(){
    return next;
  }

  public void setNext(Nodo_vacant next){
    this.next = next;
  } 
//  Nodo Vacant  //    
}
