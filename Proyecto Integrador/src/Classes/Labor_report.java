package Classes;
/**
 * @author libardo
 */
public class Labor_report{
  String concept;
  String memorandum;
  String call_attention;
  String date;
  Boolean delay;
  Employee employee_id;

  public Labor_report(String concept, String memorandum, String call_attention, String date, Boolean delay, Employee employee_id){
     this.concept = concept;
     this.memorandum = memorandum;
     this.call_attention = call_attention;
     this.date = date;
     this.delay = delay;
     this.employee_id = employee_id;
  }

    public String getConcept(){
      return concept;
    }

    public void setConcept(String concept){
      this.concept = concept;
    }

    public String getMemorandum(){
      return memorandum;
    }

    public void setMemorandum(String memorandum){
      this.memorandum = memorandum;
    }

    public String getCall_attention(){
      return call_attention;
    }

    public void setCall_attention(String call_attention){
      this.call_attention = call_attention;
    }

    public String getDate(){
      return date;
    }

    public void setDate(String date){
      this.date = date;
    }

    public Boolean getDelay(){
      return delay;
    }

    public void setDelay(Boolean delay){
      this.delay = delay;
    }

    public Employee getEmployee_id(){
      return employee_id;
    }

    public void setEmployee_id(Employee employee_id){
      this.employee_id = employee_id;
    }
  
    @Override
    public String toString(){
        return "Concept: " + concept + "\nMemorandum: " + memorandum + "\nCall attention: " + call_attention + "\nDate: " + date + "\nDelay: " + delay;
    } 
}
