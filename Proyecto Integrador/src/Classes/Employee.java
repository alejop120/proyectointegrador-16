package Classes;
/**
 * @author libardo
 */
public class Employee{
   public String name;
   public String surname;
   public String email;
   public String birthdate;
   public int phoneNumber;
   public String address;
   public int documentNumber;
   public User user_id;

    public Employee(String name, String surname, String emial, String birthdate, int phoneNumber, String address, int documentNumber, User user_id) {
      this.name = name;
      this.surname = surname;
      this.email = emial;
      this.birthdate = birthdate;
      this.phoneNumber = phoneNumber;
      this.address = address;
      this.documentNumber = documentNumber;
      this.user_id = user_id;
    }

    public String getName(){
      return name;
    }

    public void setName(String name){
      this.name = name;
    }

    public String getSurname(){
      return surname;
    }

    public void setSurname(String surname){
      this.surname = surname;
    }

    public String getEmail(){
      return email;
    }

    public void setEmail(String email){
      this.email = email;
    }
    
    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public int getPhoneNumber(){
      return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber){
      this.phoneNumber = phoneNumber;
    }

    public String getAddress(){
      return address;
    }

    public void setAddress(String address){
      this.address = address;
    }

    public int getDocumentNumber(){
      return documentNumber;
    }

    public void setDocumentNumber(int documentNumber){
      this.documentNumber = documentNumber;
    }

    public User getUser_id(){
      return user_id;
    }

    public void setUser_id(User user_id){
      this.user_id = user_id;
    }
   
    @Override
    public String toString(){
      return "Name: " + name + "\nSurname: " + surname + "\nEmail: " + email + "\nBirthdate: " + birthdate + "\nPhone: " + phoneNumber + "\nAddress: " + address + "\nDocument: " + documentNumber;
    } 
}
