package Classes;
/**
 * @author libardo
 */
public class Cv_company{
  String name;
  String surname;
  int document;
  String position;
  String requested_position;
//  Vacant vacant_id;

    public Cv_company(String name, String surname, int document, String position, String requested_position) {
      this.name = name;
      this.surname = surname;
      this.document = document;
      this.position = position;
      this.requested_position = requested_position;
    }

//    public String getFile(){
//      return file;
//    }
//
//    public void setFile(String file){
//      this.file = file;
//    }

    public String getName(){
      return name;
    }

    public void setName(String name){
      this.name = name;
    }

    public String getSurname(){
      return surname;
    }

    public void setSurname(String surname){
      this.surname = surname;
    }

    public int getDocument(){
      return document;
    }

    public void setDocument(int document){
      this.document = document;
    }

    public String getPosition(){
       return position;
    }

    public void setPosition(String position){
      this.position = position;
    }

    public String getRequested_position(){
      return requested_position;
    }

    public void setRequested_position(String requested_position){
      this.requested_position = requested_position;
    }

//    public Vacant getVacant_id(){
//      return vacant_id;
//    }
//
//    public void setVacant_id(Vacant vacant_id){
//      this.vacant_id = vacant_id;
//    }
  
    @Override
    public String toString(){
      return "Name: " + name + "\nSurname: " + surname + "\nDocument: " + document + "\nPosition: " + position + "\nRequested position: " + requested_position;
    }
}
