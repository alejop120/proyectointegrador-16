package Classes;
/**
 * @author libardo
 */
public class Endowment{
    public String name;
    public String size;
    public Boolean delivered;
    public Contract contract_id;

    public Endowment(String name, String size, Boolean delivered, Contract contract_id) {
       this.name = name;
       this.size = size;
       this.delivered = delivered;
       this.contract_id = contract_id;
    }

    public String getName(){
      return name;
    }

    public void setName(String name){
      this.name = name;
    }

    public String getSize(){
      return size;
    }

    public void setSize(String size){
      this.size = size;
    }

    public Boolean getDelivered(){
      return delivered;
    }

    public void setDelivered(Boolean delivered){
      this.delivered = delivered;
    }

    public Contract getContract_id(){
      return contract_id;
    }

    public void setContract_id(Contract contract_id){
      this.contract_id = contract_id;
    }

    @Override
    public String toString(){
      return "Name: " + name + "\nSize: " + size + "\nDelivered: " + delivered;
    }
}
