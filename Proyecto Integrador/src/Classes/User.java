package Classes;
/**
 * @author libardo sánchez
 */
public class User{
  public String name;
  public String surname;
  public String document;
  public String email;
  public String role;

  public User(String name, String surname, String document, String role){
   this.name = name;
   this.surname = surname;
   this.document = document;
   this.role = role;
  }
  
  public String getName(){
    return name;
  }

  public void setName(String name){
    this.name = name;
  }

  public String getSurname(){
    return surname;
  }

  public void setSurname(String surname){
    this.surname = surname;
  }

  public String getDocument(){
    return document;
  }

  public void setDocument(String document){
    this.document = document;
  }

  public String getRole(){
    return role;
  }

  public void setRole(String role){
    this.role = role;
  }
  
  @Override
  public String toString(){
    return "Name: " + name + "\nSurname: " + surname + "\nNº Document: " + document + "\nRole: " + role;
  }
}

