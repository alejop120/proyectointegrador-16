package Classes;
/**
 * @author libardo
 */
public class Contract{
    public String position;
    public String date;
    public int salary;
//    public Employee employee_id;
//    public Type_contract type_id;

    public Contract(String position, String date, int salary) {
      this.position = position;
      this.date = date;
      this.salary = salary;
//      this.employee_id = employee_id;
//      this.type_id = type_id;
    }

    public String getPosition(){
      return position;
    }

    public void setPosition(String position){
      this.position = position;
    }

    public String getDate(){
      return date;
    }

    public void setDate(String date){
      this.date = date;
    }

    public int getSalary(){
      return salary;
    }

    public void setSalary(int salary){
      this.salary = salary;
    }

//    public Employee getEmployee_id(){
//      return employee_id;
//    }
//
//    public void setEmployee_id(Employee employee_id){
//      this.employee_id = employee_id;
//    }
//
//    public Type_contract getType_id(){
//      return type_id;
//    }
//
//    public void setType_id(Type_contract type_id){
//      this.type_id = type_id;
//    }
    
    @Override
    public String toString(){
        return "Position: " + position + "\nDate: " + date + "\nSalary: " + salary;
    }
}
