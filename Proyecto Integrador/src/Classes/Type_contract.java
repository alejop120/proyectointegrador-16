package Classes;
/**
 * @author libardo
 */
public class Type_contract{
    String name;
    String description;

    public Type_contract(String name, String description){
      this.name = name;
      this.description = description;
    }

    public String getName(){
      return name;
    }

    public void setName(String name){
      this.name = name;
    }

    public String getDescription(){
      return description;
    }

    public void setDescription(String description){
      this.description = description;
    }
    
    @Override
    public String toString(){
      return "Name: " + name + "\nDescription: " + description;
    }
}
