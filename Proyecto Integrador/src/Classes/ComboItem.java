package Classes;
/**
 * @author josealejandro
 */
public class ComboItem{
    public String key;
    public String id;

    public ComboItem(String key, String id){
      this.key = key;
      this.id = id;
    }

    @Override
    public String toString(){
      return key;
    }

    public String getKey(){
      return key;
    }

    public String getId(){
      return id;
    }
}
