package Classes;
/**
 * @author libardo sánchez
 */
public class Nodo_doble{
   public User data;
   public Nodo_doble next;
   public Nodo_doble previous;

   public Nodo_doble(User data){
    this.data = data;
    next = null;
    previous = null;
   }
}
