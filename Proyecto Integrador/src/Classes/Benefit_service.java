package Classes;
/**
 * @author libardo
 */
public class Benefit_service{
  String arl;
  String eps;
  String pension_severance;
  String compensation_fund;
//  Employee employee_id;

    public Benefit_service(String arl, String eps, String pension_severance, String compensation_fund){
      this.arl = arl;
      this.eps = eps;
      this.pension_severance = pension_severance;
      this.compensation_fund = compensation_fund;
//      this.employee_id = employee_id;
    }

    public String getArl(){
      return arl;
    }

    public void setArl(String arl){
      this.arl = arl;
    }

    public String getEps(){
      return eps;
    }

    public void setEps(String eps){
      this.eps = eps;
    }

    public String getPension_severance(){
      return pension_severance;
    }

    public void setPension_severance(String pension_severance){
      this.pension_severance = pension_severance;
    }

    public String getCompensation_fund(){
      return compensation_fund;
    }

    public void setCompensation_fund(String compensation_fund){
      this.compensation_fund = compensation_fund;
    }

//    public Employee getEmployee_id(){
//      return employee_id;
//    }
//
//    public void setEmployee_id(Employee employee_id){
//      this.employee_id = employee_id;
//    }
  
    @Override
    public String toString(){
      return "ARL: " + arl + "\nEPS: " + eps + "\nPension severance: " + pension_severance + "\nCompensation fund: " + compensation_fund;
    }
}
