package Classes;
/**
 * @author libardo
 */
public class Vacant{
  String name;
  String position;
  String description;
  String area;
  User user_id;
    
//  User user_id
    public Vacant(String name, String position, String description, String area){
      this.name = name;
      this.position = position;
      this.description = description;
      this.area = area;
//      this.user_id = user_id;
    }

    public String getName(){
      return name;
    }

    public void setName(String name){
      this.name = name;
    }

    public String getPosition(){
      return position;
    }

    public void setPosition(String position){
      this.position = position;
    }

    public String getDescription(){
      return description;
    }

    public void setDescription(String description){
      this.description = description;
    }

    public String getArea(){
      return area;
    }

    public void setArea(String area){
      this.area = area;
    }

//    public User getUser_id(){
//      return user_id;
//    }
//
//    public void setUser_id(User user_id){
//      this.user_id = user_id;
//    }
  
    @Override
    public String toString(){
      return "Name: " + name + "\nPosition: " + position + "\nDescription: " + description + "\nArea: " + area;
    }   
}
