package Classes;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author libardo
 */
public class ConexionSQL {

    public Connection conexion, conexionToMysql;
//  ConexionSQL conexion = new ConexionSQL(); // button test

    public ConexionSQL() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/human_resources", "root", "Mysql-2016");
            conexionToMysql = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/mysql", "root", "Mysql-2016");
            System.out.println("Conexion exitosa.");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Conexion fracasada: " + e);
        }
    }

    public void newRegister(String tableName, String[] fields) {
        try {
            if (tableName.equals("user")) {
                String sql = "INSERT INTO user (user_id, name, surname, email, documentNumber, role) VALUES (?,?,?,?,?,?)";
                String lastId = "SELECT * FROM user ORDER BY user_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);

                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);
                pst.setString(4, fields[2]);
                pst.setString(5, fields[3]);
                pst.setString(6, fields[4]);

                int n = pst.executeUpdate();

                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("employee")) {
                String sql = "INSERT INTO employee (employee_id, name, surname, documentNumber, email, phoneNumber, address) VALUES (?,?,?,?,?,?,?)";
                String lastId = "SELECT * FROM employee ORDER BY employee_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);

                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);
                pst.setString(4, fields[2]);
                pst.setString(5, fields[3]);
                pst.setString(6, fields[4]);
                pst.setString(7, fields[5]);

                int n = pst.executeUpdate();

                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("contract")) {
                String sql = "INSERT INTO contract (contract_id, date, salary, position, typecontract_id, employee_id) VALUES (?,?,?,?,?,?)";
                String lastId = "SELECT * FROM contract ORDER BY contract_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);
                
                SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                Date date = formatter.parse(fields[0]);
                SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
                                
                pst.setString(1, Integer.toString(id));
                pst.setString(2, toFormat.format(date));
                pst.setString(3, fields[1]);
                pst.setString(4, fields[2]);
                pst.setString(5, fields[3]); // type
                pst.setString(6, fields[4]);

                int n = pst.executeUpdate();

                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("typecontract")) {
                String sql = "INSERT INTO "+ tableName + " (typeContract_id, name, description) VALUES (?,?,?)";
                String lastId = "SELECT * FROM " + tableName + " ORDER BY typeContract_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);
                
                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);

                int n = pst.executeUpdate();
                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("endowment")) {
                String sql = "INSERT INTO " + tableName + " (endowment_id, name, size, delivered, contract_id) VALUES (?,?,?,?,?)";
                String lastId = "SELECT * FROM " + tableName + " ORDER BY endowment_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);

                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);
                pst.setString(4, fields[2]);
                pst.setString(5, fields[3]);

                int n = pst.executeUpdate();
                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("payment")) {
                String sql = "INSERT INTO " + tableName + " (payment_id, concept, value, paymentDate, observations, refunded, employee_id) VALUES (?,?,?,?,?,?,?)";
                String lastId = "SELECT * FROM " + tableName + " ORDER BY payment_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }
                
                SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                Date date = formatter.parse(fields[2]);
                SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");

                PreparedStatement pst = conexion.prepareStatement(sql);

                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);
                pst.setString(4, toFormat.format(date));
                pst.setString(5, fields[3]);
                pst.setString(6, fields[4]);
                pst.setString(7, fields[5]);

                int n = pst.executeUpdate();
                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("descargo")) {
                String sql = "INSERT INTO "+ tableName + " (descargo_id, concept, call_attention, delay, employee_id) VALUES (?,?,?,?,?)";
                String lastId = "SELECT * FROM " + tableName + " ORDER BY descargo_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);

                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);
                pst.setString(4, fields[2]);
                pst.setString(5, fields[3]);


                int n = pst.executeUpdate();

                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
            if (tableName.equals("benefit_service")) {
                String sql = "INSERT INTO " + tableName + " (benefit_service_id, arl, eps, pension_severance, compensation_fund, employee_id) VALUES (?,?,?,?,?,?)";
                String lastId = "SELECT * FROM " + tableName + " ORDER BY benefit_service_id DESC LIMIT 1";
                System.out.println(sql);
                Statement statement = conexion.createStatement();

                int id = 1;
                ResultSet rsLastId = statement.executeQuery(lastId);
                if (rsLastId.next()) {
                    id = rsLastId.getInt(1) + 1;
                }

                PreparedStatement pst = conexion.prepareStatement(sql);

                pst.setString(1, Integer.toString(id));
                pst.setString(2, fields[0]);
                pst.setString(3, fields[1]);
                pst.setString(4, fields[2]);
                pst.setString(5, fields[3]);
                pst.setString(6, fields[4]);

                int n = pst.executeUpdate();
                if (n > 0) {
                    System.out.println("Registro exitoso");
                }
            }
        } catch (Exception e) {
            System.out.println("Error al crear registro:" + e);
        }
    }

    public void updateRow(String tableName, String id, String[] data) {
        try {
            if(tableName.equals("user")){
                String sql = "UPDATE " + tableName + " SET name='" + data[0] + "', surname='" + data[1] + "', email='" + data[2] + "', documentNumber='" + data[3] + "', role='" + data[4] + "' WHERE user_id=" + id;
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if(tableName.equals("employee")){
                String sql = "UPDATE " + tableName + " SET name='" + data[0] + "', surname='" + data[1] + "', documentNumber='" + data[2] + "', email='" + data[3] + "', phoneNumber='" + data[4] + "', address= '"+  data[5] + "' WHERE employee_id=" + id;
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if (tableName.equals("contract")) {
                String sql = "";
                if(data[0].length() > 12){
                  SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                  Date date = formatter.parse(data[0]);
                  SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
                  sql = "UPDATE " + tableName + " SET date='" + toFormat.format(date) + "', salary='" + data[1] + "', position='" + data[2] + "', typecontract_id='" + data[3] + "', employee_id='" + data[4] + "' WHERE contract_id=" + id;
                }else{
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = formatter.parse(data[0]);
                    sql = "UPDATE " + tableName + " SET date='" + formatter.format(date) + "', salary='" + data[1] + "', position='" + data[2] + "', typecontract_id='" + data[3] + "', employee_id='" + data[4] + "' WHERE contract_id=" + id;
                }
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if (tableName.equals("typecontract")) {
                String sql = "UPDATE " + tableName + " SET name='" + data[0] + "', description='" + data[1] + "' WHERE typeContract_id=" + id;
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if (tableName.equals("endowment")) {
                String sql = "UPDATE " + tableName + " SET name='" + data[0] + "', size='" + data[1] + "', delivered= '" + data[2] + "' WHERE endowment_id=" + id;
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if (tableName.equals("payment") ) {
                String sql = "";
                if (data[2].length() > 12) {
                    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                    Date date = formatter.parse(data[2]);
                    SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
                    sql = "UPDATE " + tableName + " SET concept='" + data[0] + "', value='" + data[1] + "', paymentDate= '" + toFormat.format(date) + "',observations= '" + data[3] + "', refunded = '" + data[4] + "' WHERE payment_id=" + id;
                }else{
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = formatter.parse(data[2]);
                    sql = "UPDATE " + tableName + " SET concept='" + data[0] + "', value='" + data[1] + "', paymentDate= '" + formatter.format(date) + "',observations= '" + data[3] + "', refunded = '" + data[4] + "' WHERE payment_id=" + id;
                }
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if (tableName.equals("descargo")) {
                String sql = "UPDATE " + tableName + " SET concept='" + data[0] + "', call_attention='" + data[1] + "', delay= '" + data[2] + "' WHERE descargo_id=" + id;
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
            if (tableName.equals("benefit_service")) {
                String sql = "UPDATE " + tableName + " SET arl='" + data[0] + "', eps='" + data[1] + "', pension_severance= '" + data[2] +  "', compensation_fund= '" + data[3] + "' WHERE benefit_service_id=" + id;
                System.out.println(sql);
                PreparedStatement query = conexion.prepareStatement(sql);
                query.execute();
            }
        } catch (Exception e) {
            System.out.println("Error al actualizar registro:" + e);
        }
    }

    public void removeRow(String table_name, String id) {
        try {
            String sql = "DELETE FROM " + table_name + " WHERE " + table_name + "_id=?";
            System.out.println(sql);
            PreparedStatement test = conexion.prepareStatement(sql);
            test.setString(1, id);
            test.execute();
        } catch (Exception e) {
            System.out.println("Error al eliminar registro:" + e);
        }
    }

    public String[][] allTable(String tableName) {

        String[][] datos = new String[0][0];

        try {

            Statement consult = conexion.createStatement();

            String allTable = "select * from " + tableName;
            String registers = "select count(*) from " + tableName;
            String columns = " SELECT COUNT(*)\n"
                    + "       FROM INFORMATION_SCHEMA.COLUMNS\n"
                    + "       WHERE table_schema = 'human_resources'\n"
                    + "             AND table_name = " + "'" + tableName + "'";

            ResultSet countRegisters = consult.executeQuery(registers);

            int quantityRegisters = 0;
            while (countRegisters.next()) {
                quantityRegisters = countRegisters.getInt(1);
            }

            ResultSet countColumns = consult.executeQuery(columns);

            int quantityColumns = 0;
            while (countColumns.next()) {
                quantityColumns = countColumns.getInt(1);
            }

//          if (tableName == "user"){
//            quantityColumns = quantityColumns - 1;  // Para passwrod no mostrar
//          }
            ResultSet result = consult.executeQuery(allTable);

            datos = new String[quantityRegisters][quantityColumns];

            int cont = 0;
            while (result.next()) {
                for (int i = 0; i < quantityColumns; i++) {
                    datos[cont][i] = result.getString(i + 1);
                }
                cont += 1;
            }

        } catch (Exception e) {
            System.out.println("Error: "+e);
        }
        return datos;
    }
    
    public String[][] tableRelation(String tableName, int id) {

        String[][] datos = new String[0][0];
        
        try {
            
            String allTable = "";
            String registers = "";
            
            if(tableName.equals("contract")){
                allTable = "select * from " + tableName + " WHERE employee_id=" + id;
                registers = "select count(*) from " + tableName + " WHERE employee_id=" + id;
            }
            
            if (tableName.equals("endowment")){
                allTable = "select * from " + tableName + " WHERE contract_id=" + id;
                registers = "select count(*) from " + tableName + " WHERE contract_id=" + id;
            }
            
            if (tableName.equals("payment")) {
                allTable = "select * from " + tableName + " WHERE employee_id=" + id;
                registers = "select count(*) from " + tableName + " WHERE employee_id=" + id;
            }
            
            if (tableName.equals("descargo")) {
                allTable = "select * from " + tableName + " WHERE employee_id=" + id;
                registers = "select count(*) from " + tableName + " WHERE employee_id=" + id;
            }
            
            if (tableName.equals("benefit_service")) {
                allTable = "select * from " + tableName + " WHERE employee_id=" + id;
                registers = "select count(*) from " + tableName + " WHERE employee_id=" + id;
            }

            Statement consult = conexion.createStatement();
            
            String columns = " SELECT COUNT(*)\n"
                    + "       FROM INFORMATION_SCHEMA.COLUMNS\n"
                    + "       WHERE table_schema = 'human_resources'\n"
                    + "             AND table_name = " + "'" + tableName + "'";

            ResultSet countRegisters = consult.executeQuery(registers);

            int quantityRegisters = 0;
            while (countRegisters.next()) {
                quantityRegisters = countRegisters.getInt(1);
            }

            ResultSet countColumns = consult.executeQuery(columns);

            int quantityColumns = 0;
            while (countColumns.next()) {
                quantityColumns = countColumns.getInt(1);
            }

            ResultSet result;
            result = consult.executeQuery(allTable);

            datos = new String[quantityRegisters][quantityColumns];

            int cont = 0;
            while (result.next()) {
                for (int i = 0; i < quantityColumns; i++) {
                    datos[cont][i] = result.getString(i + 1);
                }
                cont += 1;
            }

        } catch (Exception e) {
            System.out.println("Error: "+ e);
        }
        return datos;
    }
}
