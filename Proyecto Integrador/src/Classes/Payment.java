package Classes;
/**
 * @author libardo
 */
public class Payment{
  String concept;
  String observations;
  int value;
  String date;
  Boolean refunded;
//  Employee employee_id;

  public Payment(String concept, String observations, int value, String date, Boolean refunded) {
    this.concept = concept;
    this.observations = observations;
    this.value = value;
    this.date = date;
    this.refunded = refunded;
//    this.employee_id = employee_id;
  }

  public String getConcept(){
    return concept;
  }

  public void setConcept(String concept){
    this.concept = concept;
  }

  public String getObservations(){
    return observations;
  }

  public void setObservations(String observations){
    this.observations = observations;
  }

  public int getValue(){
    return value;
  }

  public void setValue(int value){
    this.value = value;
  }

  public String getDate(){
    return date;
  }

  public void setDate(String date){
    this.date = date;
  }

  public Boolean getRefunded(){
    return refunded;
  }

  public void setRefunded(Boolean refunded){
    this.refunded = refunded;
  }

//  public Employee getEmployee_id(){
//    return employee_id;
//  }
//
//  public void setEmployee_id(Employee employee_id){
//    this.employee_id = employee_id;
//  }
  
  @Override
  public String toString() {
    return "Concept: " + concept + "\nObservations: " + observations + "\nValue: " + value + "\nDate: " + date + "\nRefunded: " + refunded;
  }
}
