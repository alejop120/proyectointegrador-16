package Classes;
/**
 * @author libardo sánchez
 */
public class Nodo{
  public User data;

  public Nodo next;
  public int key;
 
// Nodo User  //
  public Nodo(User data){
   this.data = data;
   next = null;
  }
  
  public Nodo(User data, int key){
    this.data = data;
    this.key = key;
    next = null;
  }
  
  public Object getData(){
    return data;
  }

  public void setData(User data){
    this.data = data;
  }

  public Nodo getNext(){
    return next;
  }

  public void setNext(Nodo next){
    this.next = next;
  } 
// Nodo User  //
}
